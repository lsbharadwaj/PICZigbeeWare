#include <IdentifyCluster.h>
#include <pic16/stdio.h>
#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <XbeeSend.h>
static unsigned int IdentifyTime = 0;
void IdentifyQueryReply(ExplicitRxPktType *ZigbeeData)
{
    unsigned char *DestWriteCharPtr, *SrcReadCharPtr;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;

    DestWriteCharPtr =  ZigbeeTxFrame.TxData_Checksum;
    SrcReadCharPtr = (char *)&ZClPtr->ZCLFrameCtrl;

    ZClPtr->ZCLFrameCtrl.Direction ^= 0xff;
    ZClPtr->CmndId = 0x00;

    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy frametype
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy TransactionSeq Number
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++;    //Copy commandType

    SrcReadCharPtr = (char *)&IdentifyTime;
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++;    //Copy commandType
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++;    //Copy commandType
    ZigbeeTxFrame.Length = 5;
    FillHeader(ZigbeeData,1);
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
void IdentifyClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData)
{
    int *identify;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;
    switch(ZClPtr->CmndId)
        {
        case 0x00:
            identify = (int*)ZClPtr->ZCLData;
            IdentifyTime = *identify;
            break;
        case 0x01:
            if(IdentifyTime > 0)
                IdentifyQueryReply(ZigbeeData);
            break;
        default:
        }
}

void IdentifyClustSetTime(unsigned int time)
{
    IdentifyTime = time;
}

void IdentifyClustPeriodic()
{
    if(IdentifyTime > 0)
        IdentifyTime --;
}

void IdentifyQueryClient(unsigned char EpInd)
{
    unsigned char i;
    unsigned char tempChar;
    tempChar = EpInd;
    ZigbeeTxFrame.SrcEp = tempChar;
    ZigbeeTxFrame.DestEp = tempChar;
    ZigbeeTxFrame.clusterId = 0x0003;
    ZigbeeTxFrame.ProfileId = 0x0104;
    FillHeader(NULL,2);
    ZigbeeTxFrame.TxData_Checksum[0] = 0x01;
    ZigbeeTxFrame.TxData_Checksum[1] = 0x01;
    ZigbeeTxFrame.TxData_Checksum[2] = 0x01;
    ZigbeeTxFrame.Length = 3;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
