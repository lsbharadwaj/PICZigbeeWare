#include <ClusterOnOff.h>
#include <pic16/stdio.h>
#include <ZigbeeExternVar.h>
#include <eepromAccess.h>
#include <XbeeSend.h>
#include <ZigbeeGeneralCommands.h>

void OnOffClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData)
{
    unsigned char data[1];
    unsigned char eepromAddress;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;

    eepromAddress = modules[EpInd].attrib[0];
    switch(ZClPtr->CmndId)
        {
        case 0x00:
            data[0] = 0x00;
            break;
        case 0x01:
            data[0] = 0x01;
            break;
        case 0x02:
            eepromRead(eepromAddress,data,1);
            if(data[0] > 0x01)  //Needs initialization
                data[0] = 0x00;
            else
                data[0] ^= 0x01;
            break;
        default:
            //Wont come here
        }
    eepromWrite(eepromAddress,data,1);
}

void OnOffClient(char EpInd, char Cmd)
{
    unsigned char tempChar;
    ZigbeeTxFrame.SrcEp = EpInd;
    ZigbeeTxFrame.clusterId = 0x0006;
    ZigbeeTxFrame.ProfileId = 0x0104;
    tempChar = FillHeader(NULL,3);
    if(tempChar != 0xff)
        {
            ZigbeeTxFrame.TxData_Checksum[0] = 0x01;
            ZigbeeTxFrame.TxData_Checksum[1] = 0x01;
            ZigbeeTxFrame.TxData_Checksum[2] = Cmd;
            ZigbeeTxFrame.Length = 3;
            SendExplicitAddPkt(&ZigbeeTxFrame);
        }
    else
    {

    }
}
