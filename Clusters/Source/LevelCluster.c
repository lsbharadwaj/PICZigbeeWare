#include <LevelCluster.h>
#include <GenericStructures.h>
#include <eepromAccess.h>
#include <XbeeSend.h>
#include <ZigbeeGeneralCommands.h>
#include <stdio.h>

void LevelClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData)
{
    unsigned char eepromAddress, data[1], wOnOff = 0, commandHandled = 0;

    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;

    switch(ZClPtr->CmndId)
        {
        case 0x04:
            wOnOff =  1;
        case 0x00:
            data[0] = ZClPtr->ZCLData[0];
            eepromAddress = modules[EpInd].attrib[1];
            eepromWrite(eepromAddress,data,1);
            if(wOnOff == 1)
                {
                    if (data[0] != 0)
                        {
                            data[0] = 1;
                        }
                    eepromAddress = modules[EpInd].attrib[0];
                    eepromWrite(eepromAddress,data,1);
                }

            break;
        default:
            //all the rest of the commands that i wont handle. No memory
        }

}
void LevelClient(char EpInd, char Cmd)
{
    unsigned char eepromAddress, data[1];
    unsigned char tempChar;

    eepromAddress = modules[EpInd].attrib[0];
    eepromRead(eepromAddress,data,1);
    ZigbeeTxFrame.SrcEp = EpInd;
    ZigbeeTxFrame.clusterId = 0x0008;
    ZigbeeTxFrame.ProfileId = 0x0104;
    tempChar = FillHeader(NULL,3);
    if(tempChar != 0xff)
        {
            ZigbeeTxFrame.TxData_Checksum[0] = 0x01;
            ZigbeeTxFrame.TxData_Checksum[1] = 0x01;
            ZigbeeTxFrame.TxData_Checksum[2] = Cmd;
            ZigbeeTxFrame.TxData_Checksum[3] = data[0];
            ZigbeeTxFrame.TxData_Checksum[4] = 0;
            ZigbeeTxFrame.TxData_Checksum[5] = 0;
            ZigbeeTxFrame.Length = 6;
            SendExplicitAddPkt(&ZigbeeTxFrame);
        }
    else
        {

        }

}
