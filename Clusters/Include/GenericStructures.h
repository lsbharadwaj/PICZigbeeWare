#ifndef __GENERICSTRUCTURES_H_INCLUDED
#define __GENERICSTRUCTURES_H_INCLUDED
#include <XbeeTypeDef.h>
typedef void (*FnPtr)(unsigned char EpNo,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData);
typedef struct
{
    unsigned char EPBufDataParsed:1;
    unsigned char ATBufDataParsed:1;
}ZigbeeBufFlagsType;

enum accessSpec
{
    readOnly = 0,
    writeOnly,
    readWrite
};

enum ClustDirection
{
    InClust = 0,
    OutClust
};

typedef union
{
    struct{
    unsigned char EpPresent:1;
    unsigned char EpDataChangedFlag :1;
    };
    unsigned char flag;
}valueChangeFlagType;

typedef struct
{
    unsigned int attribID;
    unsigned char type;
    unsigned char minVal;
    unsigned char maxVal;
    unsigned char Access;
    unsigned char DataSize;
    void *eepromAdd;
} attributeType;


typedef struct
{
    unsigned int clusterID;
    unsigned char attribCnt;
    unsigned char ClustDir;
    FnPtr CmndHandler;      //Remember to start this at a word boundary otherwise wont work
    attributeType **attrib;
}genericClusterType;

typedef struct
{
    unsigned char EpNo;
    unsigned char ClusterCnt;
    FnPtr DevHandler;
    unsigned int profileId;
    unsigned int DeviceId;
    unsigned char DeviceVer;
    unsigned char dataPending;
    char *InputBufferAddr;
    valueChangeFlagType *EPFlag;
    genericClusterType ** cluster;
}genericZigbeeEpType;

typedef struct
{
    unsigned char FrameType:2;
    unsigned char ManSpecCode:1;
    unsigned char Direction:1;
    unsigned char DisDynResp:1;
    unsigned char reserved :3;
}ZCLFrameCtrlType;
typedef struct
{
    ZCLFrameCtrlType ZCLFrameCtrl;
    unsigned char TranSeqno;
    unsigned char CmndId;
    unsigned char ZCLData[37];
}ZCLClusterDataType;

typedef union
{
    struct
    {
    unsigned char OnOffPresent :1;
    unsigned char LevelPresent :1;
    };
    unsigned char flag;
}ClustFlagType;

typedef struct
{
    valueChangeFlagType EpFlags;
    ClustFlagType InClustFlags;
    ClustFlagType OutClustFlags;
    unsigned char attrib[2];
}moduleInfoType;

#endif // __GENERICSTRUCTURES_H_INCLUDED
