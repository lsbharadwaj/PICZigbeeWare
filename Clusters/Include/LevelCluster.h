#ifndef __CLUSTER_LEVELCLUSTER_H_INCLUDED
#define __CLUSTER_LEVELCLUSTER_H_INCLUDED
#include <ZigbeeExternVar.h>

void LevelClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData);
void LevelClient(char EpInd, char Cmd);
#endif // __CLUSTERONOFF_H_INCLUDED

