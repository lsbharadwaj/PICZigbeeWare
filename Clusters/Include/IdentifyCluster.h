#ifndef __IDENTIFYCLUSTER_H_INCLUDED
#define __IDENTIFYCLUSTER_H_INCLUDED
#include <XbeeTypeDef.h>
#include <GenericStructures.h>
extern void IdentifyClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData);
extern void IdentifyClustPeriodic();
extern void IdentifyQueryClient(unsigned char EpInd);
extern void IdentifyClustSetTime(unsigned int time);
#endif // __IDENTIFYCLUSTER_H_INCLUDED
