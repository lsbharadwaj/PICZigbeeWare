#ifndef __CLUSTERONOFF_H_INCLUDED
#define __CLUSTERONOFF_H_INCLUDED
#include <GenericStructures.h>

extern void OnOffClusterServer(unsigned char EpInd,unsigned char ClusterInd, ExplicitRxPktType *ZigbeeData);
extern void OnOffClient(char EpInd, char Cmd);
#endif // __CLUSTERONOFF_H_INCLUDED
