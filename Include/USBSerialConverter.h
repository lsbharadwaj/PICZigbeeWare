#ifndef __USBSERIALCONVERTER_H_
#define __USBSERIALCONVERTER_H_
#ifdef USE_USB_LIB
#include <USBGlobalExtern.h>
#include <ZigbeeExternVar.h>
#include <usart.h>
#include <USBCore.h>
#include <pic16/stdio.h>
void USB2Serial();
void Serial2USB();
#endif // USE_USB_LIB
#endif // __USBSERIALCONVERTER_H_
