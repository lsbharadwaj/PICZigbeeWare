/**@file main.h
*@brief This header shall set all the device configuration bits as well as includes the header files needed for main.c
*@author Bharad
*/
#ifndef __MAIN_H
#define __MAIN_H
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif
#include <pic16/pic18f4550.h>
#include <pic16/delay.h>
#include <pic16/usart.h>
#include <pic16/stdio.h>
#include <ZigbeeCore.h>
#include <mmi.h>
#include <moduleDetails.h>

//------------Set the Configuration bits here -------------------------------//
/* Disable Xinst, Set the PLLDIV and USBDIV for proper clock speed for USB. Enable the VReg for USB
* Set the Clock source with HSPLL, Enable Power On Timer, and disable Stack OverFlow Reset*/
#pragma config XINST = OFF, PLLDIV = 5, WDT = OFF, USBDIV = 2, CPUDIV=OSC1_PLL2
#pragma config FOSC = HSPLL_HS, PWRT = ON, VREGEN=ON, STVREN = OFF
#pragma config LVP = OFF

//--------------------------------------------------------------------------//

#endif // __MAIN_H
