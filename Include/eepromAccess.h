#ifndef __EEPROMACCESS_H_INCLUDED
#define __EEPROMACCESS_H_INCLUDED

extern void eepromWrite(const char eepromAddrs,const char *srcAddrsStrt, const char noOfBytes) ;
extern void eepromRead(char eepromAddrs,char *destAddrStart, char noOfBytes) ;

#endif // __EEPROMACCESS_H_INCLUDED
