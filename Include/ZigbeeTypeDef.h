#ifndef __ZIGBEETYPEDEF_H_INCLUDED
#define __ZIGBEETYPEDEF_H_INCLUDED
#include <GenericStructures.h>
#include <ZigbeeTypeDef.h>
//typedef struct
//{
//    int clusterID;
//    char attribCnt;
//    FnPtr CmndHandler;
//    __code attributeType *attrib[2];
//}clusterType;

typedef struct
{
    unsigned char SrcEp;
    unsigned int SrcClust;
    unsigned char DestAddr[8];
    unsigned int DestClust;
    unsigned int DestProfileId;
    unsigned char DestEp;
}RecordType;
#endif // __ZIGBEETYPEDEF_H_INCLUDED
