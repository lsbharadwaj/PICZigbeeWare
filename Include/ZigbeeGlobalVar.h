#ifndef __ZIGBEEGLOBALVAR_H_
#define __ZIGBEEGLOBALVAR_H_
#include <USBTypedef.h>
#include <GenericStructures.h>
#include <ZigbeeTypeDef.h>
#include <ZigbeeEndpoint1.h>
#include <ZigbeeEndpoint0.h>
#include <XbeeTypeDef.h>

__code __at 0x7f00 RecordType Record[MAX_RECORD_COUNT];

__code genericZigbeeEpType *__code ZigbeeEpList[] = {&ZigbeeEP0,&ZigbeeEP1} ;

unsigned char EPRxBuf[sizeof(ExplicitRxPktType)] = {0};
ExplicitAddressFrameType ZigbeeTxFrame;

ZigbeeBufFlagsType ZigbeeBufFlags = {0xff};
char ZigbeeMode; //< If 1 usb 2 serial & vice versa converter mode else regular mode
ringBufType SerialRingBuf;

moduleInfoType modules[ZIGBEE_MAX_ENDPOINT];
#endif // __ZIGBEEGLOBALVAR_H_
