#ifndef BUTTONDOWNCALLBACK_H_INCLUDED
#define BUTTONDOWNCALLBACK_H_INCLUDED
#include <Commissioning.h>
#include <mmi.h>
#include <OnOffLightSwitch.h>
typedef void (*BtnFnPtr)(unsigned char btn);

BtnFnPtr BtnDownFnc[4] = {handleBind,handleUp, handleDown, emptyFunc};
BtnFnPtr BtnLngPressFnc[4] = {AllowJoin,handleUp, handleDown, emptyFunc};
BtnFnPtr BtnExtendedPressFnc[4] = {ChangeNetwork,handleUp, handleDown, factoryReset};

#endif // BUTTONDOWNCALLBACK_H_INCLUDED
