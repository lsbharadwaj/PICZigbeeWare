#ifndef __FLASHACCESS_H_INCLUDED
#define __FLASHACCESS_H_INCLUDED

#include <ZigbeeExternVar.h>
#include <ZigbeeTypeDef.h>
#include <pic16/pic18f4550.h>

extern void updateRecord(RecordType *Rcrd, unsigned char delAll);
#endif // __FLASHACCESS_H_INCLUDED
