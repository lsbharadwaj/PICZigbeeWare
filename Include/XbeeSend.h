#ifndef __XBEE_SEND_H_INCLUDED
#define __XBEE_SEND_H_INCLUDED

#include <ZigbeeExternVar.h>
#include <XbeeTypeDef.h>

extern char FillHeader(ExplicitRxPktType *ZigbeeData,char LoopBack);
extern void flipBytes(char* DataPtr, char size);
extern void ATSend(unsigned char *Cmnd, unsigned char *Value, unsigned char size, char isQueued);
#endif // __XBEE_SEND_H_INCLUDED
