#ifndef __ZIGBEEEXTERNVAR_H_
#define __ZIGBEEEXTERNVAR_H_
#include <USBTypedef.h>
#include <GenericStructures.h>
#include <ZigbeeTypeDef.h>
#include <XbeeTypeDef.h>

#define ZIGBEE_MAX_ENDPOINT 9
#define MAX_RECORD_COUNT 16

__code extern __at 0x7f00 RecordType Record[MAX_RECORD_COUNT];
__code extern genericZigbeeEpType *__code ZigbeeEpList[2];
extern unsigned char EPRxBuf[] ;
extern ExplicitAddressFrameType ZigbeeTxFrame;
extern ZigbeeBufFlagsType ZigbeeBufFlags;
extern char ZigbeeMode;
extern ringBufType SerialRingBuf;
extern moduleInfoType modules[ZIGBEE_MAX_ENDPOINT];
#endif // __ZIGBEEEXTERNVAR_H_
