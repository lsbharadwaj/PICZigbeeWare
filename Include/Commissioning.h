#ifndef __COMMISSIONING_H_INCLUDED
#define __COMMISSIONING_H_INCLUDED

void AllowJoin(unsigned char btn);
void ChangeNetwork(unsigned char btn);
void CommissionTarget(unsigned char btn);
void CommissionInitiator(unsigned char btn, unsigned char start);

#endif // __COMMISSIONING_H_INCLUDED
