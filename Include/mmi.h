#ifndef MMI_H_INCLUDED
#define MMI_H_INCLUDED
#include <pic16/pic18f4550.h>

void handleUp(unsigned char btn);
void handleDown(unsigned char btn);
void handleBind(unsigned char btn);
void emptyFunc(unsigned char btn);
void factoryReset(unsigned char btn);
void callClient(unsigned char btn);
void UpdateDeviceState();


#endif // MMI_H_INCLUDED
