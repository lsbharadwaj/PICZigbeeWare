#ifndef __XBEETYPEDEF_H_INCLUDED
#define __XBEETYPEDEF_H_INCLUDED
typedef struct
{
    char RxOngoing :1;
    char EscapeChar :1;
    char DataSizeKnwn :1;
} XbeeRxFlags;

typedef struct
{
    unsigned int length;
    unsigned char FrameType;
    unsigned char FrameId;
    unsigned char ATCmd[2];
    unsigned char CmdStatus;
    unsigned char CmdData_CheckSum[40];
    unsigned char Checksum;
}AtCommandRespType;

typedef struct
{
    unsigned char StartDelimiter;
    unsigned int length;
    unsigned char FrameType;
    unsigned char FrameId;
    unsigned char ATCmd[2];
    unsigned char CmdData_CheckSum[40];
    unsigned char Checksum;
}AtCommand;


typedef struct
{
    unsigned int length;
    unsigned char FrameType;
    unsigned char Status;
    unsigned char Checksum;
} ModemStatusType;

typedef struct
{
    unsigned int length;
    unsigned char FrameType;
    unsigned char SrcAddrs[8];
    unsigned char SrcNetAddrs[2];
    unsigned char SrcEp;
    unsigned char DestEp;
    unsigned int clusterId;
    unsigned int ProfileId;
    unsigned char RxOptions;
    unsigned char RxData_Checksum[40];
} ExplicitRxPktType;

typedef struct
{
    unsigned int length;
    unsigned char FrameType;
    unsigned char SrcAddrs[8];
    unsigned char SrcNetAddrs[2];
    unsigned char RxOptions;
    unsigned char NoOfSamp;
    unsigned char DigMask;
    unsigned char AnalgMask;
    unsigned int  DigSamp;
    unsigned char AnlgData[40];
}IODataSampRxType;

typedef struct
{
    unsigned char StartDelimiter;
    unsigned int Length;
    unsigned char FrameType;
    unsigned char FrameId;
    unsigned char DestAddrs[8];
    unsigned char DestNetAddrs[2];
    unsigned char SrcEp;
    unsigned char DestEp;
    unsigned int clusterId;
    unsigned int ProfileId;
    unsigned char BroadCastRadius;
    unsigned char TxOptions;
    unsigned char TxData_Checksum[40];
}ExplicitAddressFrameType;
#endif // __XBEETYPEDEF_H_INCLUDED
