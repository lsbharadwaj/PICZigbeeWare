#ifndef __ZIGBEECORE_H_
#define __ZIGBEECORE_H_
#include <pic16/pic18f4550.h>
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBSerialConverter.h>
#include <USBCore.h>
#endif
#include <ZigbeeExternVar.h>
#include <XbeeTypeDef.h>
#include <ZigbeeGeneralCommands.h>
#include <GenericStructures.h>
#include <ZDOHandler.h>
#include <mmi.h>

#define ZCL_READ_ATTRIB_REQ 0X00

void ZigbeeInit();
void ZigbeeHandle();
void XbeeRec();
void XbeeInit();
#endif // __ZIGBEECORE_H_
