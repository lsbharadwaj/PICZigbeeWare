#include <flashAccess.h>
#include <stdio.h>
#include <USBCore.h>
void flashWrite(char *ipData,unsigned char Count)
{
    unsigned char i,j,ctr = 0;
    unsigned char temp[64];
    //Erase 64 bytes
    __asm
    movlw LOW(_Record)
    movwf _TBLPTRL
    movlw HIGH(_Record)
    movwf _TBLPTRH
    movlw UPPER(_Record)
    movwf _TBLPTRU
    __endasm;

    //Update TBLPTR to point to the actual location
    for(i = 0 ; i < Count; i ++)
        {
            TBLPTRL += 16;
            if (STATUSbits.C == 1)
                {
                    TBLPTRH++;
                    if(STATUSbits.C == 1)
                        {
                            TBLPTRU++;
                        }
                }
        }
    //And with 0xffffc0 to get the block address
    TBLPTRL = TBLPTRL & 0xc0;
    //Read 64 bytes
    for( i = 0 ; i < 64 ; i ++)
        {
            __asm
            TBLRD*+
            __endasm;
            temp[i] = TABLAT;
        }
    //Update the Ram with the record
    //Get the update location
    ctr = Count & 0x03;
    ctr = ctr * 16;
    for( i = 0 ; i < 16 ; i++)
        {
            temp[ctr++] = *ipData;
            ipData ++;
        }

    //Again update the TBLPTR for erase
    __asm
    movlw LOW(_Record)
    movwf _TBLPTRL
    movlw HIGH(_Record)
    movwf _TBLPTRH
    movlw UPPER(_Record)
    movwf _TBLPTRU
    __endasm;

    //Update TBLPTR to point to the actual location
    for(i = 0 ; i < Count; i ++)
        {
            TBLPTRL += 16;
            if (STATUSbits.C == 1)
                {
                    TBLPTRH++;
                    if(STATUSbits.C == 1)
                        {
                            TBLPTRU++;
                        }
                }
        }
    TBLPTRL = TBLPTRL & 0xc0;
    EECON1bits.EEPGD = 1;
    EECON1bits.CFGS = 0;
    EECON1bits.WREN = 1;
    EECON1bits.FREE = 1;

    //Disable interrupts for the write
    __critical
    {
        EECON2 = 0x55;
        EECON2 = 0xaa;
        EECON1bits.WR = 1;
    }
    //Write to flash
    ctr = 0;
    for(j = 0; j < 2; j++)
        {
            for(i = 0 ; i < 32; i ++)
                {
                    TABLAT = temp[ctr];
                    ctr++;
                    __asm
                    TBLWT*+
                    __endasm;
                }
            __asm
            TBLRD*-
            __endasm;
            EECON1bits.EEPGD = 1;
            EECON1bits.CFGS = 0;
            EECON1bits.WREN = 1;
            //Disable interrupts here
            __critical
            {
                EECON2 = 0x55;
                EECON2 = 0xaa;
                EECON1bits.WR = 1;
            }
            __asm
            TBLRD*+
            __endasm;
        }
}

void updateRecord(RecordType *Rcrd, unsigned char delAll)
{
    unsigned char i,Rcrdno = 0xff, tempDest, loopCount, j;
    unsigned char *SrcPtr, *DestPtr;
    if (delAll == 1)
        {
            loopCount = MAX_RECORD_COUNT;
        }
    else
        {
            loopCount = 1;
            for (i = 0 ; i < MAX_RECORD_COUNT ; i++)
                {
                    if(Rcrd->SrcEp == Record[i].SrcEp && Rcrd->SrcClust == Record[i].SrcClust )
                        {
                            Rcrdno = i;
                            break;
                        }
                }
        }

    for(j = 0; j < loopCount ; j++)
        {
            if(delAll == 1)
                {
                    Rcrdno = j ;
                }
            //if Record found
            if(Rcrdno != 0xff)
                {
                    //dest Ep same as that in the record delete it
                    if((Record[Rcrdno].DestEp == Rcrd->DestEp) || (delAll == 1))
                        {
                            Rcrd->SrcEp = 0xff;
                            Rcrd->DestEp = 0xff;
                        }
                    else //Update only the destEp
                        {
                            tempDest = Rcrd->DestEp;
                            DestPtr = (char *)(&Rcrd->SrcEp);
                            SrcPtr = (char *)(&Record[Rcrdno].SrcEp);
                            for(i = 0; i < sizeof(RecordType); i++)
                                {
                                    *DestPtr ++ = *SrcPtr ++;
                                }
                            Rcrd->DestEp = tempDest;
                        }
                }
            else//If record not found find a empty slot one whose Ep = 0xff
                {
                    for (i = 0 ; i < MAX_RECORD_COUNT ; i++)
                        {
                            if(Record[i].SrcEp == 0xff )
                                {
                                    Rcrdno = i;
                                    break;
                                }
                        }
                }
            flashWrite((char *) Rcrd,Rcrdno);
        }
}
