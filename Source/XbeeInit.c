#include <GenericStructures.h>
#include <ZigbeeExternVar.h>
#include <pic16/stdio.h>
#include <pic16/pic18f4550.h>
#include <XbeeSend.h>

/** Initilaize the RAM memory which stores the EEPROM address of the attribute to point to the proper memory
* There is a variable in the EP structure stored in the ROM which points to memory in RAM. This inturn points to EEPROM
--------------------      -----------------------    --------------------------
|ROM Structure      |    |RAM                   |   | EEPROM                  |
|                   |    |                      |   |                         |
|   *Pointer to RAM |--> |  *Address of EEPROM--|-->|    Stores attrib Value  |
|                   |    |                      |   |                         |
--------------------      ----------------------     --------------------------

* Why?
* 1) The function which writes to eeprom needs a pointer which is present in RAM
* 2) Initialization necessary as RAM wont store data permanently
*
* How?
* 1) Address is computed using the knowledge that the EEPROM memory is used contiguously by the endpoints
* 2) Each attribute is of size mentioned in the endpoint details
*
* Also
* This function initlializes the timer for 1 sec
*/
void XbeeInit()
{
    char i = 0, j = 0, k = 0;
    __code genericZigbeeEpType *__code *EPpointer;
    genericClusterType  **ClusterPtr;
    attributeType **attributePtr;
    char clusterCnt = 0;
    char attribCount = 0;
    unsigned char flag, mask;
    unsigned char address = 0x00;
    unsigned char modAttrib = 0;

    EPpointer = &ZigbeeEpList[1];

    for( i = 0 ; i < ZIGBEE_MAX_ENDPOINT; i++)
        {
            mask = 0x01;
            modAttrib = 0;
            flag = modules[i].InClustFlags.flag| modules[i].OutClustFlags.flag;
            if(modules[i].EpFlags.EpPresent == 0)
                continue;

            clusterCnt = (*EPpointer)->ClusterCnt;
            ClusterPtr = (*EPpointer)->cluster;
            ClusterPtr ++;
            // Running a loop is creating problem so had to run it this way!!!
            for (j = 0 ; j < clusterCnt; j++,ClusterPtr ++)
                {

                    attribCount = (*ClusterPtr)->attribCnt;
                    attributePtr = (*ClusterPtr)->attrib;
                    //                printf("cluster 1 = %d\n", attribCount);
                    for (k = 0; k < attribCount; k++,attributePtr++)
                        {
                            if(flag & mask != 0)
                                {
                                    modules[i].attrib[modAttrib++] = address;
                                    //All the hardwork of parsing through the endpoint is to update the eeprom address below
                                    address += (*attributePtr)->DataSize;
                                    //                        printf("address = %d\n",i);
                                }
                            else
                                {
                                    modules[i].attrib[modAttrib++] = 0xff;
                                }
                        }
                }

            mask = mask << 1;
        }

//      Setup the TIMER0
    INTCONbits.T0IE = 1;
    INTCON2bits.T0IP = 0;
    TMR0H = 0x48;   //1sec period
    TMR0L = 0xe5;
    T0CON = 0b10000110;

}

