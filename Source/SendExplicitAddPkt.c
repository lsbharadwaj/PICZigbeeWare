#include <pic16/stdio.h>
#include <pic16/usart.h>
#include <ZigbeeGeneralCommands.h>
#include <XbeeSend.h>
void flipBytes(char* DataPtr, char size)
{
    char temp, cntr,loop;
    loop = size/2;
    for (cntr = 0; cntr < loop; cntr++)
        {
            temp = DataPtr[cntr];
            DataPtr[cntr] = DataPtr[size - cntr - 1];
            DataPtr[size - cntr - 1] = temp;
        }
}
/*This function shall compelte the packet for transmit and send it over uart to XBEE and return once completed
* sending
* 1) Correct the length to be the sum of the length + the header size
* 2) Compute checksum and send data over UART
* 3) Return when the data is sent
* @input SendPkt with the length field filled with the size of DataPayload in bites
*/
void SendExplicitAddPkt(ExplicitAddressFrameType *SendPkt) __critical
{

    static unsigned char Id = 0;
    unsigned int counter ;
    unsigned int length ;
    unsigned char *charPtr = (char*)(&(SendPkt->FrameType));
    char sum = 0;

    //Set the necessary variables
    SendPkt->StartDelimiter = 0x7E;
    SendPkt->FrameType = 0x11;
    SendPkt->FrameId = Id++;
    SendPkt->Length += 20;   // 20 is the number of bytes before the payload
    //Save the length as the bytes shall be flipped
    length = SendPkt->Length ;

    //Compute Checksum
    for (counter = 0 ; counter < length ; counter++)
        {
            sum += *charPtr;
            charPtr++;
        }
    sum = 0xff - sum;
    *charPtr = sum; //Save the Checksum

    //Change the order of the bytes for length
    flipBytes((char*)&(SendPkt->Length),2);
    charPtr = (char*)((SendPkt));

//    Transfer the data to UART
    for (counter = 0 ; counter < length + 4; counter++)
        {
            while( usart_busy() );
            usart_putc( *charPtr );
            charPtr++;
        }
}
