#include <USBSerialConverter.h>
#ifdef USE_USB_LIB
void USB2Serial()
{
    char updatedStart;
    //calculate the next index using modulo
    updatedStart = USBRingBuf.startInd + 1;
    if(updatedStart > USBRingBuf.sizeInd)
        updatedStart = 0;

    //wait for the data to arrive if no data
    if(updatedStart != USBRingBuf.endInd)
        {
            // copy the return data and update the index
            usart_putc(USBRingBuf.buf[updatedStart]);
            USBRingBuf.startInd = updatedStart;
        }
}

void Serial2USB()
{
    char updatedStart;
    //calculate the next index using modulo
    updatedStart = SerialRingBuf.startInd + 1;
    if(updatedStart > SerialRingBuf.sizeInd)
        updatedStart = 0;

    //wait for the data to arrive if no data
    if(updatedStart != SerialRingBuf.endInd)
        {
            // copy the return data and update the index
            stdout = STREAM_USER;
            printf("%c",SerialRingBuf.buf[updatedStart]);
            USBflush();
            SerialRingBuf.startInd = updatedStart;
        }

}
#endif // USE_USB_LIB
