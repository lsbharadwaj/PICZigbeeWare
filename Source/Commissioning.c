#include<pic16/pic18f4550.h>
#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <XbeeSend.h>
#include <pic16/stdio.h>
#include <flashAccess.h>
#include <ZigbeeTypeDef.h>
#include <IdentifyCluster.h>
#include <ClusterOnOff.h>

#define REPLY_WAIT_TIME 30
unsigned char CommisioningStage = 0;
unsigned char CommissioningEp = 0xff;
unsigned char DestEpList[8];
unsigned char CompletedDestEpCnt = 0, TotalComEpCnt = 0;
unsigned char Time;

void AllowJoin(unsigned char btn)
{
    RecordType rcrd;
    unsigned char arr[2];
    unsigned char i;
    unsigned char destAdd[8]= {0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff};
    ZigbeeTxFrame.clusterId = 0x0036;
    ZigbeeTxFrame.ProfileId = 0x0000;
    FillHeader(NULL,2);
    ZigbeeTxFrame.TxData_Checksum[0] = 0x01;
    ZigbeeTxFrame.TxData_Checksum[1] = 180;
    ZigbeeTxFrame.TxData_Checksum[2] = 0x00;
    ZigbeeTxFrame.Length = 3;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}

void ChangeNetwork(unsigned char btn)
{
    unsigned char arr[] = {0x00};
    RecordType rcrd;
    ATSend("nr",arr,1,0);
}


/** This is a periodic function which handles Commissioning in On Off Light Switch
* There are 3 different stages in this
* 1) 0 = Uncommisioned
* 2) 1 = Identify Query Sent
* 3) 2 = Sent Match Desc
* 4) 3 = Matching Descriptor
* This responds to the state of commissioning stage
*/
void CommissioningPeriodic()
{
    RecordType rcrd;
    static char atmpt = 0;
    // Decrement time every second when this periodic function is called
    if(Time != 0)
        Time --;
    // Either commissioning never started or was turned off
    if(CommisioningStage == 0)
        {
            // Reset all variables
            CompletedDestEpCnt = 0;
            TotalComEpCnt = 0;
            CommissioningEp = 0xff;
            atmpt = 0;
        }
    // Identify request sent
    if(CommisioningStage == 1)
        {
            if(Time == 0)
                {
                    // Raise the attempt and send Identify query and wait for 10 ticks
                    atmpt ++;
                    Time = REPLY_WAIT_TIME;
                    IdentifyQueryClient(CommissioningEp);
                }
            // Stop commissioning if there has been more than 3 attemps and no reply
            if(atmpt > 2)
                {
                    CommisioningStage = 0;
                }
        }
    // Match Desc req sent
    else if(CommisioningStage == 2)
        {
            // atmpt > 0 means a reply to identify cluster was received
            if(atmpt > 0)
                {
                    // Wait for REPLY_WAIT_TIME sec for reply of match desc
                    Time = REPLY_WAIT_TIME;
                    // Set atmpt = 0 so that we dont end up setting time to 5 again
                    atmpt = 0;
                }
            // if time elapsed without match desc result then stop commisioning
            if(Time == 0)
                {
                    CommisioningStage = 0;
                }
        }
    // Match Desc reply received then toggle each light one by one till you find the proper ep
    else if(CommisioningStage == 3)
        {
            // Wait for the time to elapse before toggling the next switch
            if(Time == 0)
                {

                    CompletedDestEpCnt++;
                    // Check if all the Eps submitted by match desc is tested
                    if (CompletedDestEpCnt == TotalComEpCnt)
                        {
                            // If all the eps tested then stop commissioning
                            // Also delete the Endpoint record
                            CommisioningStage = 0;
                            rcrd.SrcEp = CommissioningEp;
                            rcrd.SrcClust = 0x0006;
                            CompletedDestEpCnt --;
                            rcrd.DestEp = DestEpList[CompletedDestEpCnt];
                            updateRecord(&rcrd, 0) ;   //Delete record
                            rcrd.SrcClust = 0x0008;
                            updateRecord(&rcrd, 0) ;   //Update record
                        }
                    else
                        {
                            // Test the next endpoint
                            rcrd.DestEp = DestEpList[CompletedDestEpCnt];
                            rcrd.SrcClust = 0x0006;
                            rcrd.SrcEp = CommissioningEp;
                            updateRecord(&rcrd, 0) ;   //Update record
                            rcrd.SrcClust = 0x0008;
                            updateRecord(&rcrd, 0) ;   //Update record
                            Time = REPLY_WAIT_TIME;
                        }
                }
            OnOffClient(CommissioningEp,0x02);
        }
    else
        {
        }

}

//if reply to identify query then send match desc
//If reply from match desc write the record
void CommissioningReplyHndlr(unsigned char EpNo,unsigned char isZDO, ExplicitRxPktType *ZigbeeData)
{
    unsigned char tempChar, *charPtr;
    char i;
    RecordType rcrd;
    //The command is from cluster Lib
    if(isZDO == 0)
        {
            // If home automation profile and the cluster is identify then send a match desc req
            if(ZigbeeData->ProfileId == 0x0104 && ZigbeeData->clusterId == 0x0003 && CommisioningStage == 1)
                {
                    //Send Match Desc
                    ZigbeeTxFrame.ProfileId = 0x0000;
                    ZigbeeTxFrame.clusterId = 0x0006;
                    FillHeader(ZigbeeData, 1);
                    charPtr = ZigbeeTxFrame.TxData_Checksum;
                    *charPtr ++ = 0x01;
                    *charPtr ++ = ZigbeeTxFrame.DestNetAddrs[0] ;
                    *charPtr ++ = ZigbeeTxFrame.DestNetAddrs[1] ;
                    *charPtr ++ = 0x04;
                    *charPtr ++ = 0x01;
                    if(modules[CommissioningEp].OutClustFlags.LevelPresent)
                        {
                            *charPtr ++ = 0x02;
                            *charPtr ++ = 0x06;
                            *charPtr ++ = 0x00;
                            *charPtr ++ = 0x08;
                            *charPtr ++ = 0x00;
                            *charPtr ++ = 0x00;
                            ZigbeeTxFrame.Length = 11;
                        }
                    else if (modules[CommissioningEp].OutClustFlags.OnOffPresent)
                        {
                            *charPtr ++ = 0x01;
                            *charPtr ++ = 0x06;
                            *charPtr ++ = 0x00;
                            *charPtr ++ = 0x00;
                            ZigbeeTxFrame.Length = 9;
                        }
                    else
                        {
                        }
                    SendExplicitAddPkt(&ZigbeeTxFrame);
                    //Set Commissioning stage to having sent the Match Desc req
                    CommisioningStage = 2;
                }
        }
    else
        {
            // if the match desc req reply arrived
            if(ZigbeeData->clusterId == 0x8006 && CommisioningStage == 2)
                {
                    charPtr = ZigbeeData->RxData_Checksum;
                    charPtr++;
                    if(*charPtr == 0x00)
                        {
                            charPtr += 3;
                            // Store the total number of EPs in the variable
                            TotalComEpCnt = *charPtr++;
                            // Handle a max of 8 Eps
                            if(TotalComEpCnt > 8)
                                TotalComEpCnt = 8;
                            // Update the EP list for which the match desc has been applied. This list will be used
                            // to toggle the LEDS in the periodic service
                            for(i = 0 ; i < TotalComEpCnt; i++)
                                {
                                    DestEpList[i] = *charPtr++;
                                }
                            // Update the record so that the address can be used
                            for(i = 0; i < 8; i++)
                                {
                                    rcrd.DestAddr[i] = ZigbeeData->SrcAddrs[i];
                                }
//                            rcrd.DestClust = 0x0006;
                            rcrd.DestEp = DestEpList[0];
                            rcrd.SrcEp =  CommissioningEp;
                            rcrd.SrcClust = 0x0006;
                            rcrd.DestProfileId = 0x0104;
                            updateRecord(&rcrd, 0);
                            rcrd.SrcClust = 0x0008;
                            updateRecord(&rcrd, 0);
                            Time = REPLY_WAIT_TIME;
                            CommisioningStage = 3;
                        }
                    else
                        {
                            // if Match desc doesnt match then stop commisioning
                            CommisioningStage = 0;
                        }
                }
        }
}

/** This function starts comissioning by acting as initiator. In this mode the device will initiate query
* The Endpoint is set as the CommissioningEp EP
*/
void CommissionInitiator(unsigned char btn, unsigned char start)
{

    CommissioningEp = btn ;
    CommisioningStage = start;
}

/** In this mode the device acts as target. It simply sets timer on. Any identifyquery request will be replied
* from this momemnt on to 0x01ff
*/
void CommissionTarget(unsigned char btn)
{
    IdentifyClustSetTime(0x01ff);
}
