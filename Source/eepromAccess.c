#include <pic16/pic18f4550.h>
#include <pic16/delay.h>
void eepromWrite(const char eepromAddrs,const char *srcAddrsStrt, const char noOfBytes) __critical
{
    char i,tempEECon;
    tempEECon = EECON1;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS = 0;
    EECON1bits.WREN = 1;
    for( i = 0 ; i < noOfBytes; i++)
    {
        EEDATA = *(srcAddrsStrt + i);
        EEADR = eepromAddrs + i;
        EECON2 = 0x55;
        EECON2 = 0xAA;
        EECON1bits.WR = 1;
        while(EECON1bits.WR);
        if(EECON1bits.WRERR == 1)
        {
            while(1)
            {
//            LATA = LATA^0xff;
            delay100ktcy(100);
            }
        }
    }
//    EECON1 = tempEECon;
}

void eepromRead(char eepromAddrs,char *destAddrStart, char noOfBytes) __critical
{
    char i,tempEECon;
    tempEECon = EECON1;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS = 0;
    for (i  = 0 ;i < noOfBytes ; i++)
    {
        EEADR = eepromAddrs + i;
        EECON1bits.RD = 1;
        while(EECON1bits.RD);
        *(destAddrStart + i) = EEDATA;
    }
    EECON1 = tempEECon;
}
