#include <XbeeSend.h>
#include <pic16/stdio.h>
#include <pic16/usart.h>
/* For commands that set values there shall be no waiting for the response.
If the size of the data is 0 then it implies trying to read data. the response data will be
returned in the Value field. It should be made sure that is has suffecient memeory to hold the data
*/
void ATSend(unsigned char *Cmnd, unsigned char *Value, unsigned char sizeVal, char isQueued)
{
    AtCommand *ATCmndFrame = (AtCommand *)&ZigbeeTxFrame;
    AtCommandRespType *ATCmndResp;
    unsigned char *tempCharPtr,j, checksum = 0, *tempReadPtr;
    unsigned int length = sizeVal;
    // If sizeVal is 0 then set command. No response needed
    ATCmndFrame->StartDelimiter = 0x7E;
    length += 4;
    ATCmndFrame->length = length;
    #ifdef FULL_CODE
    if(isQueued == 1)
    {
        ATCmndFrame->FrameType = 0x09;
        checksum += 0x09;
    }
    else
    {
        ATCmndFrame->FrameType = 0x08;
        checksum += 0x08;
    }
    #else
        ATCmndFrame->FrameType = 0x08;
        checksum += 0x08;
    #endif
    if(sizeVal > 0)
    {
        ATCmndFrame->FrameId = 0x00;
    }
    //else read command. Response needed
    else
    {
        ATCmndFrame->FrameId = 0x01;
        checksum ++;
    }
    //Copy the command
    tempCharPtr = ATCmndFrame->ATCmd;
    *tempCharPtr++ = *Cmnd;
    checksum += *Cmnd++;
    *tempCharPtr++ = *Cmnd; //Copy two bytes
    checksum += *Cmnd++;
    //Copy the data
    for (j = 0; j < sizeVal; j++)
    {
        *tempCharPtr ++ = *Value;
        checksum += *Value++;
    }
    //Fill the checksum
    *tempCharPtr ++ = 0xff - checksum;

    //Change the order of the bytes for length
    flipBytes((char*)&(ATCmndFrame->length),2);
    tempCharPtr = (char*)((ATCmndFrame));

//    Transfer the data to UART
    for (j = 0 ; j < length + 4; j++)
    {
      while( usart_busy() );
     usart_putc( *tempCharPtr );
     tempCharPtr++;
    }

    //wait for the reply if the data is to be read
    #ifdef FULL_CODE
    if(sizeVal == 0)
    {
        //loop till new data arrives
        while(ZigbeeBufFlags.EPBufDataParsed == 1);
        ATCmndResp = (AtCommandRespType *)EPRxBuf;
        length = ATCmndResp->length - 7;
        flipBytes((char*)&(ATCmndFrame->length),2);
        tempCharPtr = Value;
        tempReadPtr = (char *)(ATCmndResp->CmdData_CheckSum);
        for (j = 0; j < length; j++)
        {
            *tempCharPtr++ = *tempReadPtr++;
        }
        ZigbeeBufFlags.EPBufDataParsed = 1;
    }
    #endif
}
