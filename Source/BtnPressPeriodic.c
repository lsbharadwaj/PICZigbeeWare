#include <BtnPressPeriodic.h>
#include <ButtonDownCallback.h>
#include <ClusterOnOff.h>
#define SMALL_PRESS 3
#define LONG_PRESS 10
#define FULL_PRESS 0XFF
#define resetTicks 30
#define MaxSpecialKeys 4
#define MaxIpKeys 8
static unsigned char PrtButTime[MaxIpKeys];
static unsigned char specialButTime[MaxSpecialKeys];

void DetectButtonPressPeriodic()
{
    static unsigned char stopLed;
    char tempPrtData, i, mask, ctr;
    tempPrtData = PORTA;
    mask = 0x02;
    // increment stopLed and set inhibit glow when it has elapsed the time
    stopLed ++;
    if (stopLed > resetTicks)
        {
            LATCbits.LATC0 = 1;
        }
    for (i = 0; i < MaxSpecialKeys ; i++)
        {
            if((tempPrtData & mask) == 0x00) //If the button is pressed it wil be pulled low
                {
                    //evertime a button is pressed reset timer
                    stopLed = 0;
                    if(specialButTime[i] == 0xff)
                        {
                            specialButTime[i] = 0;
                        }
                    else
                        {
                            ctr = specialButTime[i];
                            ctr ++;
                            specialButTime[i] = ctr;
//                            if(specialButTime[i] < 3)
//                                {
////                                    LATE ++;
////                                    LATA = 0x01;
//                                }
//                            else if (specialButTime[i] < 6)
//                                {
////                                LATE ++;
////                                    LATA = 0x04;
//                                }
//                            else if (specialButTime[i] != 0xff)
//                                {
////                                LATE ++;
////                                    LATA = 0x10;
//                                }
//                            else
//                                {
//                                }

                        }
                }
            else // Either button released or never pressed
                {
                    if(specialButTime[i] < SMALL_PRESS)
                        {
                            BtnDownFnc[i](i);
                        }
                    else if (specialButTime[i] < LONG_PRESS)
                        {
                            BtnLngPressFnc[i](i);
                        }
                    else if (specialButTime[i] < FULL_PRESS)
                        {
                            BtnExtendedPressFnc[i](i);
                        }
                    else    //Button was never pressed
                        {

                        }
                    specialButTime[i] = 0xff;
                }
            mask = mask << 1;
        }

    tempPrtData = PORTD;
    mask = 0x01;

    for (i = 0; i < MaxIpKeys ; i++)
        {
            if((tempPrtData & mask) == 0x00) //If the button is pressed it wil be pulled low
                {
                    //evertime a button is pressed reset timer
                    if(PrtButTime[i] == FULL_PRESS)
                        {
                            PrtButTime[i] = 0;
                        }
                    else
                        {
                            ctr = PrtButTime[i];
                            ctr ++;
                            PrtButTime[i] = ctr;
                        }
                }
            else // Either button released or never pressed
                {
                    if(PrtButTime[i] < 0xff && modules[i+1].OutClustFlags.flag != 0)
                        {
                            callClient(i);
//                            printf("button %d pressed\n",i);
                        }
                    PrtButTime[i] = 0xff;
                }
            mask = mask << 1;
        }

}
