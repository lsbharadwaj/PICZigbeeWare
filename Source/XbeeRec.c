/**This function will be used as an ISR for receiving data
* and  copying the data into the EndPoints
*/
#include <pic16/usart.h>
#include <XbeeTypeDef.h>
#include <usart.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#include <ZigbeeExternVar.h>

/** This function receives the data from XBee and intimates the EP by setting the EpDataChangedFlag to 1
* Data is then stored in the corresponding buffer. Currently there is only one buffer for all the endpoints due to memory crunch
*/
void XbeeRec() __critical
{
    unsigned char i, EpCounter;
    static unsigned char RxDataStream[80] = {0};
    ExplicitRxPktType * TempDataHolder;
    static XbeeRxFlags RxFlag = {0};
    static int PendBytes, PktSize;
    unsigned char *temp,index;
    char RxData;
    RxData = usart_getc();
    // If a current data reception has not started then it is a new packet
    if(RxFlag.RxOngoing == 0)
        {
        // This if helps discard corrupt packets.
            if(RxData != 0x7E)
                {
//                    printf("corrupt packet \n");
                    // Dont start reception until a proper start of frame is detected
                }
            else    //if Rx data is 0x7E delimeter then new packet
                {
                    //Set PendBytes = 2 to read the size of the packet
                    PendBytes = 2;
                    // The next char is not an escape char
                    RxFlag.EscapeChar = 0;
                    // Start reception of packet
                    RxFlag.RxOngoing = 1;
                    // Data size is unknown
                    RxFlag.DataSizeKnwn = 0;
                }
        }
    else
        {
            // If packet reception started and the data size of the incoming packet is unknown find size
            if(RxFlag.DataSizeKnwn == 0)
                {
                    //
                    temp = (char *)&PktSize;
                    temp[PendBytes-1] = RxData;
                    PendBytes --;
                    if(PendBytes == 0)
                        {
                            RxFlag.DataSizeKnwn = 1;
                            PendBytes = PktSize;
//                            printf("pktSize = %d\n",PktSize);
                        }
                }
            else //if the packet reception is going on and packet size is known
                {
                    if(PendBytes != 0) // Keep reading till pendBytes is 0
                        {
                            //If at anytime start of frame appears init for new reception
                            // Might happen on corrupted packets
                            if( RxData == 0x7E)
                                {
                                    PendBytes = 2;
                                    RxFlag.EscapeChar = 0;
                                    RxFlag.RxOngoing = 1;
                                    RxFlag.DataSizeKnwn = 0;
                                }
                            else
                                {
                                    // find the index on the data to be put in the buffer
                                    index = PktSize - PendBytes;
                                    RxDataStream[index+2] = RxData;
                                    PendBytes --;
//                                    printf(" %d",PendBytes);
                                }
                        }
                    else // Enters here when the checksum data is received
                        {
//                            printf("completed rx \n",PendBytes);
//                            printf("********\n");
//                            for (i = 0 ; i < PktSize + 2; i++)
//                            {
//                                printf("%x ",RxDataStream[i]);
//                            }
//                            printf("********\n");
                            // Inform that the reception of the packet is complete
                            RxFlag.RxOngoing = 0;
                            // Typecast to the explicit rx pkt type
                            TempDataHolder = (ExplicitRxPktType *) RxDataStream;
                            // Check if the ep data is parsed and acted upon. Else wait
//                            if(ZigbeeBufFlags.EPBufDataParsed == 1)
                                {
                                    // 0x91 = Explicit Rx buffer
                                    if(TempDataHolder->FrameType == 0x91)
                                        {
                                            // The two bytes of the packet length field
                                            PktSize +=2;
                                            // set this as the true length
                                            TempDataHolder->length = PktSize;

                                            for(EpCounter = 0 ; EpCounter < ZIGBEE_MAX_ENDPOINT ; EpCounter++)
                                                {
                                                    // If the packet Ep matches the incoming data ep or if it is a broadcast packet then receive the data
                                                    if((EpCounter == TempDataHolder->DestEp || (TempDataHolder->RxOptions & 0x02) == 1 ) && modules[EpCounter].EpFlags.EpPresent==1)
                                                        {
                                                            if((TempDataHolder->RxOptions & 0x02) == 1)
                                                            {
                                                                EpCounter = 2;  //Send the message to EP 2 which has identify cluster
                                                            }
                                                            temp = (unsigned char *) RxDataStream;
                                                            for( i = 0 ; i < PktSize; i ++)
                                                                {
                                                                    EPRxBuf[i] = temp[i];
                                                                }
                                                            modules[EpCounter].EpFlags.EpDataChangedFlag =1;
//                                                            printf("data rec in ep %d\n", EpCounter);
                                                            // Notify that there is pending data to process
                                                            ZigbeeBufFlags.EPBufDataParsed = 0;
                                                        }
                                                }
                                        }
                                    // IF AT command
                                    else if(TempDataHolder->FrameType == 0x88)
                                        {
                                            PktSize +=2;
                                            TempDataHolder->length = PktSize;
                                            temp = (unsigned char *) RxDataStream;
                                            for( i = 0 ; i < PktSize; i ++)
                                                {
                                                    EPRxBuf[i] = temp[i];
                                                }
                                                // Notify that there is pending data to process
                                                ZigbeeBufFlags.EPBufDataParsed = 0;
                                        }
                                    else
                                        {
                                        }
                                }
                        }
                }
        }
}
