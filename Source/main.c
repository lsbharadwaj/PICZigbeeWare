#include <main.h>
#include <flashAccess.h>
#include <BtnPressPeriodic.h>
#include <IdentifyCluster.h>
#include <OnOffLightSwitch.h>
#include <LevelCluster.h>
typedef void (*PeriodicServicePtr)();

//Periodic services that shall be called every tick
PeriodicServicePtr PeriodicService[3] = {DetectButtonPressPeriodic,IdentifyClustPeriodic,CommissioningPeriodic};

static Tick = 0; //< This variable is toggled every second after which the periodic services will be serviced

/**The function initializes the controller
*/
void DeviceInit()
{
    unsigned char i;
    unsigned int tempInt, mask = 0x01;
    //--Enable PORT A leds by disabling adc
    ADCON1 = 0x0e;
    ADCON2 = 0xb8;
    //TRISA is input by default. The a0 will be made ip/op when needed
    LATAbits.LATA0 = 1;

    TRISB = 0x00;
    LATB = 0x00;

    TRISCbits.RC0 = 0;

    TRISD = 0x00;
    LATD = 0x00;
    PORTEbits.RDPU = 1; //enable pull up resistors

    TRISE = 0x00;
    LATE = 0;
    //Enable Interrupt Priority and Global Interrupt
    RCONbits.IPEN = 1;

    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;

    T2CON = 0x03; //Set timer 2 for max pre and post scale and keep timer off
//    INTCON2bits.RBPU = 0;

    //Detect the modules connected
    TRISAbits.RA0 = 1; // make pin input
    ADCON0bits.ADON = 1;
    LATCbits.LATC0 = 0;
    T2CONbits.TMR2ON = 1;
    for (i = 0; i < ZIGBEE_MAX_ENDPOINT; i++)
        {
            if (i>0)
                {
                    LATE = i - 1;
                    TMR2 = 0;
                    while(!PIR1bits.TMR2IF);
                    PIR1bits.TMR2IF = 0;
                }
            ADCON0bits.GO = 1;
            while(ADCON0bits.GO);
            tempInt = ADRESL & 0xff| ADRESH <<8;
            if (i == 0)
                {
                    modules[i].EpFlags.flag = 0x01;
                    modules[i].InClustFlags.flag = 0x00;
                    modules[i].OutClustFlags.flag = 0x00;
                }
            else if (tempInt > NO_DEVICE)
                {
                    modules[i].EpFlags.flag = 0x00;
                }
            else if (tempInt > ON_OFF_INPUT && tempInt < NO_DEVICE )
                {
                    modules[i].EpFlags.flag = 0x01;
                    modules[i].InClustFlags.flag = 0x00;
                    // If outcluster that pin will be input
                    modules[i].OutClustFlags.flag = 0x03;
                    TRISD = TRISD | mask;
                }
            else if (tempInt > ON_OFF_OUTPUT && tempInt < ON_OFF_INPUT )
                {
                    modules[i].EpFlags.flag = 0x01;
                    modules[i].InClustFlags.flag = 0x00;
                    // If outcluster that pin will be input
                    modules[i].OutClustFlags.flag = 0x03;
                    TRISD = TRISD | mask;
                }
            else if (tempInt > LEVEL_INPUT && tempInt < ON_OFF_OUTPUT )
                {
                    modules[i].EpFlags.flag = 0x01;
                    modules[i].InClustFlags.flag = 0x00;
                    // If outcluster that pin will be input
                    modules[i].OutClustFlags.flag = 0x03;
                    TRISD = TRISD | mask;
                }
            else
                {
                    modules[i].EpFlags.flag = 0x01;
                    modules[i].InClustFlags.flag = 0x00;
                    // If outcluster that pin will be input
                    modules[i].OutClustFlags.flag = 0x03;
                    TRISD = TRISD | mask;
                }
            if(i > 0)
            {
                mask = mask << 1;
            }
        }

    T2CONbits.TMR2ON = 0;
    LATCbits.LATC0 = 1;
    ADCON0bits.ADON = 0;
    TRISAbits.RA0 = 0; // Restore Pin as op
}

void main()
{
    unsigned char arr[1];
    unsigned char i,ctr;
    //Enable device related registers
    DeviceInit();
    //Enable USB related registers and variables (NEEDs IPEN and GIE to be enabled before0
#ifdef USE_USB_LIB
    UsbInit();
#endif // USE_USB_LIB
    //Configure the controller to talk to the XBEE over usart
    usart_open(
        USART_TX_INT_OFF & //0x7f
        USART_RX_INT_ON &  //0xff
        USART_BRGH_HIGH &  //0xff
        USART_EIGHT_BIT & // 0xfd
        USART_ASYNCH_MODE, //0xfe
        1249// 9600 at 48MHz
    );

    //Initialize Zigbee
    ZigbeeInit();
    //Initialize XBEE
    XbeeInit();
    //Restore the device state as it was initially present before restart
    UpdateDeviceState();
    arr[0] = 0x01;
    ATSend("ap",arr,1,0); //Allow network join for 180 secs
    arr[0]=0x03;
    ATSend("ao",arr,1,0); //ZDO Pass through. ie ZDO commands not supported will be passed to the controller
    arr[0] = 0xb4;
    ATSend("nj",arr,1,0); //Allow network join for 180 secs
    //--------------------------------------------//
    stdout = STREAM_USER; //Needed if the printf has to print output through the USB
    while(1)
        {
//            LevelClusterServer(0,0,NULL);
//            if(PIR1bits.RCIF == 1)
            //Store the XBEE messages arriving from the XBEE module to the controller
//        XbeeRec();
            //Continously handle zigbee commands
            ZigbeeHandle();
            //Check for Periodic Events
            if(Tick == 1)
                {
//            if(ctr++ > 60)
//            {
//            CommissionInitiator(2);
//            ctr = 0;
//            }
//                printf(". ");

//                XbeeInit();
//                 printf("*********\n");
//                for (i = 0 ; i < ZIGBEE_MAX_ENDPOINT ;i ++)
//                {
//                    printf("mod %d onOff %d level %d flag %x %x\n",i,modules[i].attrib[0], modules[i].attrib[1], modules[i].InClustFlags.flag, modules[i].OutClustFlags.flag);
//                }
                    //Every second service the three periodic service.
                    for(i = 0; i < 3 ; i++)
                        {
                            PeriodicService[i]();
                        }
                    Tick = 0;
                }

        }
    //--------------------------------------------//


}

void ISRH() __interrupt(1)
{
    if(PIR2bits.USBIF == 1)
        {
#ifdef USE_USB_LIB
            //Needed to handle the usb requests
            UsbInterrupHandler();
            PIR2bits.USBIF = 0;
#endif // USE_USB_LIB
        }
}

void ISRL() __interrupt(2)
{
    if(PIR1bits.RCIF == 1)
        //Store the XBEE messages arriving from the XBEE module to the controller
        XbeeRec();
    else if(INTCONbits.T0IF == 1)
        {
            //Set Tick to 1 so that the periodic services will be serviced in main code
            Tick = 1;
            //configure timer for 1 sec period
            TMR0H = 0x48;   //1sec period
            TMR0L = 0xe5;
            INTCONbits.T0IF = 0;
        }
    else
        {
        }

}
