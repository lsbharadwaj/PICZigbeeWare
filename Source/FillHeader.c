#include <XbeeSend.h>

char FillHeader(ExplicitRxPktType *ZigbeeData,char LoopBack)
{
    unsigned char j, Rcrdno = 0xff;
    unsigned char Ep;
    unsigned int clust;
    unsigned char temp;
    //----------------------
    if(LoopBack == 1)
        {
            for(j = 0; j < 8; j++)
                ZigbeeTxFrame.DestAddrs[j] = ZigbeeData->SrcAddrs[j];
            for(j = 0; j < 2; j++)
                ZigbeeTxFrame.DestNetAddrs[j] = ZigbeeData->SrcNetAddrs[j];
            temp = ZigbeeData->SrcEp;
            ZigbeeTxFrame.SrcEp = ZigbeeData->DestEp;
            ZigbeeTxFrame.DestEp = temp;
            ZigbeeTxFrame.clusterId = ZigbeeData->clusterId;
            flipBytes((char*)&ZigbeeTxFrame.clusterId,2);
            ZigbeeTxFrame.ProfileId = ZigbeeData->ProfileId;
            flipBytes((char*)&ZigbeeTxFrame.ProfileId,2);
            ZigbeeTxFrame.BroadCastRadius = 0;
            ZigbeeTxFrame.TxOptions = 0;
            //--------------------
        }
    else if(LoopBack == 2) //BroadCast
        {
            for(j = 0; j < 8; j++)
                {
                    if(j < 6)
                        ZigbeeTxFrame.DestAddrs[j] = 0x00;
                    else
                        ZigbeeTxFrame.DestAddrs[j] = 0xFF;
                }
            ZigbeeTxFrame.DestNetAddrs[0] = 0xFF;
            ZigbeeTxFrame.DestNetAddrs[1] = 0xFE;
            ZigbeeTxFrame.BroadCastRadius = 0;
            ZigbeeTxFrame.TxOptions = 0;
            flipBytes((char*)&ZigbeeTxFrame.clusterId,2);
            flipBytes((char*)&ZigbeeTxFrame.ProfileId,2);
        }
    else if(LoopBack == 3) //Find the information from the binding table
        {
            Ep = ZigbeeTxFrame.SrcEp;
            clust = ZigbeeTxFrame.clusterId;
            //find the record
            for (j = 0; j < MAX_RECORD_COUNT; j++)
                {
                    if(Ep == Record[j].SrcEp && clust == Record[j].SrcClust )
                        {
                            Rcrdno = j;
                            break;
                        }
                }
            if(Rcrdno != 0xff)
                {
                    ZigbeeTxFrame.DestEp = Record[Rcrdno].DestEp;
                    ZigbeeTxFrame.clusterId = Record[Rcrdno].DestClust;
                    for(j = 0; j < 8; j++)
                        ZigbeeTxFrame.DestAddrs[j] = Record[Rcrdno].DestAddr[j];
                    ZigbeeTxFrame.DestNetAddrs[0] = 0xFF;
                    ZigbeeTxFrame.DestNetAddrs[1] = 0xFE;
                    ZigbeeTxFrame.BroadCastRadius = 0;
                    ZigbeeTxFrame.TxOptions = 0;
                    flipBytes((char*)&ZigbeeTxFrame.clusterId,2);
                    flipBytes((char*)&ZigbeeTxFrame.ProfileId,2);
                }
        }
    else
        {
        }
        return(Rcrdno);
}
