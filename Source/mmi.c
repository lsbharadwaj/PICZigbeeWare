#include <mmi.h>
#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <stdio.h>
#include <eepromAccess.h>
#include <Commissioning.h>
#include <ClusterOnOff.h>
#include <LevelCluster.h>
#include <flashAccess.h>

void handleDown(unsigned char btn)
{
    TRISAbits.RA0 = 0;
    LATCbits.LATC0 = 0;
    if(LATE != 0)
        {
            LATE --;
        }
}

void handleUp(unsigned char btn)
{
    TRISAbits.RA0 = 0;
    LATCbits.LATC0 = 0;
    if(LATE != 7)
        {
            LATE ++;
        }
}

void handleBind(unsigned char btn)
{
    if (LATCbits.LATC0 == 0)
        {
            if (modules[LATE + 1].InClustFlags.flag)
                {
                    CommissionTarget(LATE + 1);
                }
            else
                {
                    CommissionInitiator(LATE + 1, 1);
                }
            LATCbits.LATC0 = 1;
        }
    else
        {
            CommissionInitiator(LATE + 1, 0);
        }

}

void factoryReset(unsigned char btn)
{
    RecordType rcrd;
    updateRecord(&rcrd, 1);
}

void emptyFunc(unsigned char btn)
{
}

void getData(char* data, char bits, char pin)
{
    char dir =0, i , mask = 0x01, dataBit;
    *data = 0x00;
    mask = mask << pin; //mask for the pin
    T2CONbits.TMR2ON = 1;

    for (i = 0; i < bits; i++)
        {
            // Toggle Low and High
            LATB ^= mask; //set the pin high
            //start the timer
            TMR2 = 0;
            while(!PIR1bits.TMR2IF);
            PIR1bits.TMR2IF = 0;
            LATB ^= mask; //set the pin low
            while(!PIR1bits.TMR2IF);
            PIR1bits.TMR2IF = 0;

            //load the 0th bit
            dataBit = LATD & mask; //get 0th bit
            if (dataBit) //if 1
                {
                    *data = *data << 1;
                    *data = *data | 0x01; // set the pin high
                }
        }
    T2CONbits.TMR2ON = 0; //stop timer
}

void callClient(unsigned char btn)
{
    unsigned char eepromAddress;
    unsigned char temp, i;


    i = btn + 1; //To get module no
    if (modules[i].OutClustFlags.LevelPresent == 1) // check if level cluster is present
        {
            getData(&temp, 8, i);
            //Read the ON off data
            eepromAddress = modules[i].attrib[1]; // Write the level data
            eepromWrite(eepromAddress,&temp, 1);
            if (temp)
                {
                    temp = 0x01;
                    eepromAddress = modules[i].attrib[0]; // write
                    eepromWrite(eepromAddress,&temp, 1);
                }
            else
                {
                    temp = 0;
                    eepromWrite(eepromAddress,&temp, 1);
                }
            LevelClient(btn + 1, 0x04);

        }
    else if (modules[i].OutClustFlags.OnOffPresent == 1) // check if on off is present
        {
            eepromAddress = modules[i].attrib[0]; // Read the on off data
            eepromRead(eepromAddress,&temp, 1);
            temp ^= 0x01;
            eepromWrite(eepromAddress,&temp, 1);
            OnOffClient(btn + 1,0x02);
        }
    else //no opcluster so do nothing
        {
        }
//        printf("");

}


void sendData(char data, char bits, char pin)
{
    unsigned char dir =0, i , mask = 0x01, dataBit;
    mask = mask << pin; //mask for the pin
    T2CONbits.TMR2ON = 1;
    for (i = 0; i < bits; i++)
        {
            LATB ^= mask; //set the pin high
            //start the timer
            TMR2 = 0;
//        printf("%x %x \n", LATB, LATD);
            while(!PIR1bits.TMR2IF);
            PIR1bits.TMR2IF = 0;
            //load the 0th bit
            dataBit = data & 0x01; //get 0th bit
            if (dataBit) //if 1
                {
                    LATD |= mask; // set the pin high
                }
            else
                {
                    LATD &= ~mask; //set the pin low
                }
            //start the timer
            TMR2 = 0;
            T2CONbits.TMR2ON = 1;
            LATB ^= mask; //set the pin low
            while(!PIR1bits.TMR2IF);
            PIR1bits.TMR2IF = 0;
            data = data >> 1;
//        printf("%x %x \n", LATB, LATD);

        }
    T2CONbits.TMR2ON = 0; //stop timer
}

/** Update the device state depending on the the attribute value stored in the EEPROm
*/
void UpdateDeviceState()
{
    unsigned char i;
    unsigned char eepromAddress;
    unsigned char temp;
    for (i = 1 ; i < ZIGBEE_MAX_ENDPOINT; i++)
        {
            // check if ep is present
            if (modules[i].EpFlags.EpPresent == 1)
                {
                    if (modules[i].InClustFlags.LevelPresent == 1) // check if level cluster is present
                        {
                            //Read the ON off data
                            eepromAddress = modules[i].attrib[0]; // Read the on off data
                            eepromRead(eepromAddress,&temp, 1);
                            if (temp)
                                {
                                    eepromAddress = modules[i].attrib[1]; // Read the Level data
                                    eepromRead(eepromAddress,&temp, 1);
                                    sendData(temp,8,i - 1);
                                }
                            else
                                {
                                    temp = 0;
                                    sendData(temp, 8, i - 1);
                                }

                        }
                    else if (modules[i].InClustFlags.OnOffPresent == 1) // check if on off is present
                        {
                            eepromAddress = modules[i].attrib[0]; // Read the on off data
                            eepromRead(eepromAddress,&temp, 1);
                            sendData(temp, 1, i - 1);
                        }
                    else //ipcluster so do nothing
                        {
                        }
                }
//        printf("Finished updating ... \n");
        }
}
