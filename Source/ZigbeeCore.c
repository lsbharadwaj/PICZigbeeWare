#include <ZigbeeCore.h>
#include <ZigbeeGlobalVar.h>

/**
* 1) This function sets the Interupt priority to low for USART rx and Tx
* 2) Set Zigbee for regular usage
* 3) Initializes the ring buffers
*/
void ZigbeeInit()
{
    IPR1bits.RCIP = 0;
    IPR1bits.TXIP = 0;

    //Set zigbee to be used for the product and not as USB-Serial converter
    ZigbeeMode = 0;
    // Initialize the ring buffer
    SerialRingBuf.startInd = 0;
    SerialRingBuf.endInd = 1;      //< One less byte inorder to identify the end
    SerialRingBuf.sizeInd = 23;    //< One byte less size
}

/** This function checks for availability of zigbee messages at the endpoints and services the requests
*/
void ZigbeeHandle()
{
    unsigned char found = 0;
    ExplicitRxPktType *rxPtr;
    ZCLClusterDataType *ZCLClusterPtr;
    genericClusterType  **ClusterPtr;
//    __code genericZigbeeEpType *__code *EPpointer;
    unsigned char EpCounter, ClusterCnt;
    unsigned char DataChanged;
    unsigned int count;
    unsigned char i = 0;
    stdout = STREAM_USER;
    // If the mode is for testing with data looped to USB
    if (ZigbeeMode == 1)
        {
#ifdef USE_USB_LIB
            USB2Serial();
            Serial2USB();
#endif // USE_USB_LIB
        }
    else
        {
            /**
            *   Check if the ep has new data
            *   If yes check if the profile id and the cluster id match
            *   If it matches check if the frametype is the genearl command
            *   If yes then check the command and call its function
            *   if the Profile id or the cluster id do not match then just loop back the data
            */
//            EPpointer = ZigbeeEpList;
            for(EpCounter = 0 ; EpCounter < ZIGBEE_MAX_ENDPOINT; EpCounter++)
                {
                    //Check if the endpoint is present before continuing
                    if (modules[EpCounter].EpFlags.EpPresent == 0)
                        {
                            continue;
                        }
                    __critical
                    {
                        DataChanged = modules[EpCounter].EpFlags.EpDataChangedFlag;
                    }
                    //Check if the data has changed in the ep
                    if( DataChanged == 1)   //If it has changed
                        {
//                            printf("data changed ");
                            __critical
                            {
                                rxPtr = (ExplicitRxPktType *)EPRxBuf;
                                count = rxPtr->length - 20;
                            }
                            //Check if the profile id is right
                            flipBytes((char*)&rxPtr->ProfileId ,2);
                            flipBytes((char*)&rxPtr->clusterId,2);
                            //Check if the profile id matches the profile id of the EP where it landed
//                            printf("got msg %x\n",rxPtr->ProfileId);
                            if(0x0104 == rxPtr->ProfileId) //Check if home automation profile
                                {
                                    ClusterPtr = ZigbeeEpList[1]->cluster;
                                    ClusterCnt = ZigbeeEpList[1]->ClusterCnt;

                                    //if profile id is right then loop throught the clusters to find the cluster if it exists
                                    for( i = 0 ; i < ClusterCnt; i++,ClusterPtr++)
                                        {
                                            if((*ClusterPtr)->clusterID == rxPtr->clusterId && i == 0)
                                                {
                                                    found = 1;
                                                    break;
                                                }
                                            else if((*ClusterPtr)->clusterID == rxPtr->clusterId && modules[EpCounter].attrib[i-1] != 0xff)
                                                {
                                                    found = 1;
                                                    break;
                                                }
                                            else
                                                {
                                                }
                                        }
                                }
                            else if(rxPtr->ProfileId == 0x0000)
                                {

                                    if((rxPtr->clusterId & 0x8000) == 0x0000)
                                        {
                                            found = 2; // zdo commands
                                        }
                                    else
                                        {
                                            found = 3; //zdo replies
                                        }

                                }
                            else  //If profile id does not match then do nothing
                                {
                                }
                            if (found == 2) //If not a response. Due to flipbytes 0080
                                {
#ifdef USE_ZDO
                                    ZDOHandler(rxPtr);
#endif // USE_ZDO
                                }
                            else if(found == 1) //if the cluster exists then  check what type of command using frametype
                                {
                                    ZCLClusterPtr = (ZCLClusterDataType *)rxPtr->RxData_Checksum;
                                    if(ZCLClusterPtr->ZCLFrameCtrl.Direction == 0x00)
                                        {
                                            if (ZCLClusterPtr->ZCLFrameCtrl.FrameType == 0x00) //If general commands
                                                {
#ifdef USE_ZCL
                                                    switch(ZCLClusterPtr->CmndId)   //check what command
                                                        {
                                                        case ZCL_READ_ATTRIB_REQ:
                                                            ZCLReadAttrib(EpCounter, i,rxPtr);
                                                            break;
                                                        case 0x02:
                                                            ZCLWriteAttrib(EpCounter, i,rxPtr,0);
                                                            break;
                                                        case 0x03:
                                                            ZCLWriteAttrib(EpCounter, i,rxPtr,1);
                                                            break;
                                                        case 0x05:
                                                            ZCLWriteAttrib(EpCounter, i,rxPtr,2);
                                                            break;
                                                        case 0x0c:
                                                            ZCLDiscoverAttrib(EpCounter, i,rxPtr);
                                                            break;
                                                        case 0x0b:
                                                            break;  //Dont do anything in return of a default resp
                                                        default:
                                                            ZCLDefaultResponse(EpCounter, i,rxPtr,0x82);

                                                        }
#endif

                                                }
                                            else
                                                {
                                                    (*ClusterPtr)->CmndHandler(EpCounter, i,rxPtr);
                                                }
                                        }
                                    else
                                        {
                                            ZigbeeEpList[1]->DevHandler(EpCounter, 0,rxPtr);
                                        }
                                }
                            else if(found == 3)
                                {
                                    ZigbeeEpList[1]->DevHandler(EpCounter, 1,rxPtr);
                                }
                            else    //loop back if the cluster id or the prfile id dont match
                                {
                                    //LoopBack
//                                    ZigbeeTxFrame.Length = count;
//                                    FillHeader(rxPtr,1);
//                                    for(i = 0 ; i < count ; i++)
//                                        {
//                                            ZigbeeTxFrame.TxData_Checksum[i] = rxPtr->RxData_Checksum[i];
//                                        }
//                                    SendExplicitAddPkt(&ZigbeeTxFrame);

                                }
                            __critical
                            {
                                modules[EpCounter].EpFlags.EpDataChangedFlag = 0;
                                ZigbeeBufFlags.EPBufDataParsed = 1;
                            }
                            UpdateDeviceState();
                        }
                }
        }
}
