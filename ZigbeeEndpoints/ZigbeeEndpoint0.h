#ifndef __ZIGBEEENDPOINT0_H_INCLUDED
#define __ZIGBEEENDPOINT0_H_INCLUDED

#include <ClusterOnOff.h>
#include <XbeeTypeDef.h>
#include <GenericStructures.h>
valueChangeFlagType EP0AttribChanged = {0};

__code genericZigbeeEpType ZigbeeEP0 = {    .EpNo = 0x00,
                                     .ClusterCnt = 0,
                                     .dataPending = 0,
                                     .InputBufferAddr = EPRxBuf,
                                     .EPFlag = &EP0AttribChanged,
                                     .cluster = NULL,
                                     .profileId = 0x0000
                                };


#endif // __ZIGBEEENDPOINT0_H_INCLUDED
