#ifndef __ZIGBEEENDPOINT1_H_INCLUDED
#define __ZIGBEEENDPOINT1_H_INCLUDED
#include <IdentifyCluster.h>
#include <XbeeTypeDef.h>
#include <GenericStructures.h>
#include <OnOffLightSwitch.h>
#include <LevelCluster.h>
valueChangeFlagType EP1AttribChanged = {0};
//Initialization of the attrbiute structures
__code attributeType LevelAttribSw1 =      {   .attribID = 0x0000,
                                     .type = 0x10,   //Boolean
                                     .minVal = 0x00,
                                     .maxVal = 0xfe,
                                     .Access = readOnly,
                                     .DataSize = sizeof(char) ,
                                 };
attributeType *__code  attributeLevelClustList1[] = {&LevelAttribSw1};
//Initiazliation of clusters
__code  genericClusterType LevelCluster =   {.clusterID = 0x0008,
                                           .attribCnt = 1,
                                           .CmndHandler = LevelClusterServer,
                                           .attrib = attributeLevelClustList1,
                                            };

__code attributeType OnOffAttribSw1 =   {   .attribID = 0x0000,
                                     .type = 0x10,   //Boolean
                                     .minVal = 0,
                                     .maxVal = 1,
                                     .Access = readWrite,
                                     .DataSize = sizeof(char) ,
                                 };
 attributeType *__code  attributeList1[] = {&OnOffAttribSw1};
//Initiazliation of clusters
__code  genericClusterType OnOffCluster =   {.clusterID = 0x0006,
                                           .attribCnt = 1,
                                           .CmndHandler = OnOffClusterServer,
                                           .attrib = attributeList1,
                                            };

__code  genericClusterType IdentifyClust =   {.clusterID = 0x0003,
                                           .attribCnt = 0,
                                           .CmndHandler = IdentifyClusterServer,
                                           .attrib = NULL,
                                            };
//Initialization of Endpoint
genericClusterType *__code ClusterArrayDev1[] = {&IdentifyClust,&OnOffCluster,&LevelCluster};


__code genericZigbeeEpType ZigbeeEP1 = { .EpNo = 0x01,
                                     .ClusterCnt = 3,
                                     .DevHandler = CommissioningReplyHndlr,
                                     .InputBufferAddr = EPRxBuf,
                                     .DeviceId = 0x0001,
                                     .DeviceVer = 0x01,
                                     .EPFlag = &EP1AttribChanged,
                                     .profileId = 0x0104,
                                     .cluster = ClusterArrayDev1
                                };

#endif // __ZIGBEEENDPOINT1_H_INCLUDED
