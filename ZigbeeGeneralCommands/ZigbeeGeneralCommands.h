#ifndef __ZIGBEEGENERALCOMMANDS_H_INCLUDED
#define __ZIGBEEGENERALCOMMANDS_H_INCLUDED
#include <ZigbeeExternVar.h>
#include <XbeeTypeDef.h>
#include <XbeeSend.h>
extern void SendExplicitAddPkt(ExplicitAddressFrameType *SendPkt);
extern void ZCLReadAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData);
extern void ZCLWriteAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData,char mode);
extern void ZCLDiscoverAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData);
extern void ZCLDefaultResponse(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData,unsigned char response);

#endif // __ZIGBEEGENERALCOMMANDS_H_INCLUDED
