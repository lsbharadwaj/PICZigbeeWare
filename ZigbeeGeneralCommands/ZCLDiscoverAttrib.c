#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <GenericStructures.h>
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#ifdef USE_ZCL
void ZCLDiscoverAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData)
{
    unsigned char i,j;
    genericClusterType  *ClusterPtr;
    attributeType **attributePtr;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;
    unsigned char MaxAttribRequestedCnt;
    unsigned int *StartAttribPtr, StartAttrib, attrib;
    unsigned char *DestWriteCharPtr,*SrcReadCharPtr, Count;
    unsigned char PktSize = 0;   //Shall keep a check on the size of data written
    //----------------------
    FillHeader(ZigbeeData, 1);
    //--------------------
    ZClPtr->ZCLFrameCtrl.Direction ^= 0xff;
    ZClPtr->TranSeqno = ZClPtr->TranSeqno;
    ZClPtr->CmndId += 1;

    DestWriteCharPtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadCharPtr = (char *)&ZClPtr->ZCLFrameCtrl;
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy frametype
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy TransactionSeq Number
    *DestWriteCharPtr ++ = (*SrcReadCharPtr ++ );    //Copy commandType
    PktSize += 3;

    ClusterPtr =    ZigbeeEpList[1]->cluster[ClusterInd];


    SrcReadCharPtr = ZClPtr->ZCLData;
    StartAttribPtr = (int *) SrcReadCharPtr;
    StartAttrib = *StartAttribPtr;
    SrcReadCharPtr +=2;
    MaxAttribRequestedCnt = *SrcReadCharPtr++;

    Count = ClusterPtr->attribCnt;  //No of Attributes present

    DestWriteCharPtr++;
    attributePtr = (ClusterPtr)->attrib;
    for (j = 0 ; j < Count && j < MaxAttribRequestedCnt; j++,attributePtr++)
        {
            attrib = (*attributePtr)->attribID ;
            if( attrib >= StartAttrib)
                {
                    SrcReadCharPtr = (char *)&attrib;
                    flipBytes(SrcReadCharPtr,2);

                    for(i = 0; i < 2; i++)
                        {
                            *DestWriteCharPtr ++ = *SrcReadCharPtr ++;
                            PktSize++;
                        }
                    *DestWriteCharPtr ++ = (*attributePtr)-> type;
                    PktSize++;
                }
        }
        DestWriteCharPtr = ZigbeeTxFrame.TxData_Checksum + 3;
        if(j == Count)
        {
            *DestWriteCharPtr = 1;
        }
        else
        {
            *DestWriteCharPtr = 0;
        }
        PktSize++;
    ZigbeeTxFrame.Length = PktSize;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
#endif
