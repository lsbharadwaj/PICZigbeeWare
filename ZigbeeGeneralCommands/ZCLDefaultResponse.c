#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <GenericStructures.h>
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#ifdef USE_ZCL
void ZCLDefaultResponse(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData,unsigned char response)
{
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;
    unsigned char tempCmdId;
    unsigned char *DestWriteCharPtr,*SrcReadCharPtr;
    unsigned char PktSize = 0;   //Shall keep a check on the size of data written
    //----------------------
    FillHeader(ZigbeeData, 1);
    //--------------------
    ZClPtr->ZCLFrameCtrl.Direction ^= 0xff;
    ZClPtr->TranSeqno = ZClPtr->TranSeqno;
    tempCmdId = ZClPtr->CmndId;
    ZClPtr->CmndId = 0x0b;

    DestWriteCharPtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadCharPtr = (char *)&ZClPtr->ZCLFrameCtrl;
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy frametype
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy TransactionSeq Number
    *DestWriteCharPtr ++ = (*SrcReadCharPtr ++ );    //Copy commandType
    *DestWriteCharPtr ++ = tempCmdId;
    *DestWriteCharPtr ++ = response;
    PktSize += 5;

    ZigbeeTxFrame.Length = PktSize;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
#endif
