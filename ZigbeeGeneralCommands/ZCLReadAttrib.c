#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <GenericStructures.h>
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#include <eepromAccess.h>
#ifdef USE_ZCL
void ZCLReadAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData)
{
    unsigned char j;
    genericClusterType  *ClusterPtr;
    attributeType **attributePtr;
    unsigned char eepromAddress;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;
    unsigned char AttribRequestedCnt;
    unsigned int *RequestAttribPtr;
    unsigned char *DestWriteCharPtr,*SrcReadCharPtr, Count;
    unsigned char PktSize = 0;   //Shall keep a check on the size of data written
    unsigned char Found = 0,eepromDataSize;
    unsigned char AttribCntr;
    //----------------------
    FillHeader(ZigbeeData, 1);
    //--------------------
    ZClPtr->ZCLFrameCtrl.Direction ^= 0xff;
    ZClPtr->TranSeqno = ZClPtr->TranSeqno;
    ZClPtr->CmndId += 1;

    DestWriteCharPtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadCharPtr = (char *)&ZClPtr->ZCLFrameCtrl;
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy frametype
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy TransactionSeq Number
    *DestWriteCharPtr ++ = (*SrcReadCharPtr ++ );    //Copy commandType
    PktSize += 3;

    ClusterPtr =    ZigbeeEpList[1]->cluster[ClusterInd];

    // Calculate the No of attirbutes requested
    AttribRequestedCnt  = (ZigbeeData->length - 20 - 3)/2;
    RequestAttribPtr = (int *)ZClPtr->ZCLData;
    SrcReadCharPtr = ZClPtr->ZCLData;
    Count = ClusterPtr->attribCnt;
    for(AttribCntr = 0; AttribCntr < AttribRequestedCnt; AttribCntr++,RequestAttribPtr++)
    {
        for(j = 0; j < 2; j++)
            {
                *DestWriteCharPtr ++ = *SrcReadCharPtr ++;
                PktSize++;
            }
        attributePtr = (ClusterPtr)->attrib;
        Found = 0;
        flipBytes((char*)RequestAttribPtr,2);
        for( j = 0 ; j < Count; j++,attributePtr++)
            {
                if((*attributePtr)->attribID == *RequestAttribPtr)
                    {
                        Found = 1;
                        break;
                    }
            }
        if(Found == 1)
            {
                *DestWriteCharPtr++ = 0x00;
                PktSize++;
                *DestWriteCharPtr++ = (*attributePtr)->type;
                PktSize++;
                eepromAddress = modules[EpInd].attrib[ClusterInd-1];
//                eepromAddress = (unsigned char*)(*attributePtr)->eepromAdd;
                eepromDataSize = (*attributePtr)->DataSize;
                eepromRead(eepromAddress,DestWriteCharPtr,eepromDataSize);
                PktSize += eepromDataSize ;
                DestWriteCharPtr += eepromDataSize;
            }
        else
            {
                *DestWriteCharPtr++ = 0x86;
                PktSize++;
            }
    }
    ZigbeeTxFrame.Length = PktSize;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
#endif
