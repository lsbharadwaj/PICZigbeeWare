#include <ZigbeeExternVar.h>
#include <ZigbeeGeneralCommands.h>
#include <GenericStructures.h>
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#include <eepromAccess.h>

#ifdef USE_ZCL
/* mode = 0 WriteAttrib
*       = 1 WriteAttrib Undivided
*       = 2 WriteAttrib No response
*/

unsigned char getSize(unsigned char dataType)
{
    unsigned char size = 0;
    if(dataType < 0x38)
        {
            dataType = dataType & 0x0f;
            if(dataType >= 0x08)
                {
                    dataType -= 8;
                }
            size = dataType + 1;
        }
    else if(dataType >= 0xe0)
        {
            switch (dataType)
                {
                case 0xe0:
                case 0xe1:
                case 0xe2:
                case 0xea:
                    size = 4;
                    break;
                case 0xe8:
                case 0xe9:
                    size = 2;
                    break;
                case 0xf0:
                    size = 8;
                    break;
                case 0xf1:
                    size = 16;
                    break;
                default:
                    size = 0xff;
                }
        }
    else
        {
            size = 0xff;
        }
    return (size);
}


void ZCLWriteAttrib(char EpInd,char ClusterInd, ExplicitRxPktType *ZigbeeData,char mode)
{
    unsigned char j,dataLeft, iteration;
    unsigned char AttribSize = 0;
    genericClusterType  *ClusterPtr;
    attributeType **attributePtr;
    unsigned char eepromAddress;
    ZCLClusterDataType *ZClPtr = (ZCLClusterDataType*)ZigbeeData->RxData_Checksum;
    unsigned int *RequestAttribPtr;
    unsigned char *DestWriteCharPtr,*SrcReadCharPtr, Count,*RequestAttribDataType;
    unsigned char PktSize = 0;   //Shall keep a check on the size of data written
    unsigned char Found = 0,eepromDataSize;
    unsigned char missedSomeData = 0;
    //----------------------
    FillHeader(ZigbeeData, 1);
    //--------------------

    ZClPtr->ZCLFrameCtrl.Direction ^= 0xff;
    ZClPtr->TranSeqno = ZClPtr->TranSeqno;
    ZClPtr->CmndId = 0x04;

    DestWriteCharPtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadCharPtr = (char *)&ZClPtr->ZCLFrameCtrl;
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy frametype
    *DestWriteCharPtr ++ = *SrcReadCharPtr ++ ;    //Copy TransactionSeq Number
    *DestWriteCharPtr ++ = (*SrcReadCharPtr ++ );    //Copy commandType
    PktSize = 3;

    ClusterPtr =    ZigbeeEpList[1]->cluster[ClusterInd];

    //Run twice so that write complete can be implemented
    for (iteration = 0 ; iteration < 2; iteration++)
        {
            PktSize = 3;
            // Calculate the No of attirbutes requested
            SrcReadCharPtr = (unsigned char *)&ZClPtr->ZCLData;
            dataLeft = (ZigbeeData->length - 20 -3 );
            Count = ClusterPtr->attribCnt;
            while(dataLeft != 0)
                {
                    RequestAttribPtr = (int *)SrcReadCharPtr;
                    //Copy the attribute Id
                    for(j = 0; j < 2; j++)
                        {
                            *DestWriteCharPtr ++ = *SrcReadCharPtr ++;
                            PktSize++;
                            dataLeft --;
                        }
                    attributePtr = (ClusterPtr)->attrib;
                    Found = 0;
                    flipBytes((char*)RequestAttribPtr,2);
                    for( j = 0 ; j < Count; j++,attributePtr++)
                        {
                            if((*attributePtr)->attribID == *RequestAttribPtr )
                                {
                                    Found = 1;
                                    break;
                                }
                        }
                    RequestAttribDataType =  SrcReadCharPtr++;
                    dataLeft--;
                    AttribSize = getSize(*RequestAttribDataType);
                    if(Found == 1)
                        {

                            if((*attributePtr)->type != (*RequestAttribDataType))
                                {
                                    dataLeft -= AttribSize;
                                    SrcReadCharPtr += AttribSize;
                                    *DestWriteCharPtr++ = 0x8d;
                                    missedSomeData = 1;
                                    PktSize++;
                                }
                            else if ((*attributePtr)->Access == readOnly)
                                {
                                    dataLeft -= AttribSize;
                                    SrcReadCharPtr += AttribSize;
                                    *DestWriteCharPtr++ = 0x88;
                                    missedSomeData = 1;
                                    PktSize++;
                                }
                            else if(AttribSize != (*attributePtr)->DataSize)
                                {
                                    dataLeft -= AttribSize;
                                    SrcReadCharPtr += AttribSize;
                                    *DestWriteCharPtr++ = 0x87;
                                    missedSomeData = 1;
                                    PktSize++;
                                }
                            else
                                {
                                    //If successful then  simply go back as there is no need of a response
                                    DestWriteCharPtr -= 2;
                                    PktSize -= 2;
//                                    eepromAddress = (unsigned char*)(*attributePtr)->eepromAdd;
                                    eepromAddress = modules[EpInd].attrib[ClusterInd-1];
                                    eepromDataSize = (*attributePtr)->DataSize;
                                    if(mode == 2 || mode == 0 || missedSomeData == 0)
                                        eepromWrite(eepromAddress,SrcReadCharPtr,eepromDataSize);
                                    SrcReadCharPtr += eepromDataSize;
                                    dataLeft -= eepromDataSize;
                                }

                        }
                    else
                        {
                            *DestWriteCharPtr++ = 0x86;
                            dataLeft -= AttribSize;
                            SrcReadCharPtr += AttribSize;
                            PktSize++;
                            missedSomeData = 1;
                        }
                    flipBytes((char*)RequestAttribPtr,2);
                }
        }
    if(missedSomeData == 0)
        {
            *DestWriteCharPtr++ = 0x00;
            PktSize++;
        }
    ZigbeeTxFrame.Length = PktSize;
    if(mode != 2)
        SendExplicitAddPkt(&ZigbeeTxFrame);
}
#endif


