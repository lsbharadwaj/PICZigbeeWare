#include <ZDOHandler.h>
#ifdef USE_ZDO
void SimpleDescReq(ExplicitRxPktType *ZigbeeData)
{
#if 1
    unsigned char * DestWritePtr, *SrcReadPtr, *tempPtr1, *tempIpClstCntPtr, *tempOpClstCntPtr;
    unsigned char PktSize, EPReqNo, i,j, FoundEp = 0, SimpDescLength = 0, ipClustCnt = 0, opClustCnt = 0;
    unsigned char mask = 0x01;
    genericClusterType  **ClusterPtr;
//    __code genericZigbeeEpType *__code *EPpointer;

    FillHeader(ZigbeeData, 1);
    ZigbeeTxFrame.clusterId = 0x0480;
    DestWritePtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadPtr = ZigbeeData->RxData_Checksum;
    *DestWritePtr ++ = *SrcReadPtr ++; //Copy the Id
    tempPtr1 = DestWritePtr ++;    //Skip the status will write later and store lcoation
//    Copy Nwk Address
    *DestWritePtr ++ = *SrcReadPtr ++;
    *DestWritePtr ++ = *SrcReadPtr ++;
    EPReqNo = *SrcReadPtr;
    PktSize = 4;
    if(EPReqNo == 0 || EPReqNo > 0xf1)
        {
            *DestWritePtr++ = 0;
            PktSize ++;
            DestWritePtr =  tempPtr1;
            *DestWritePtr = 0x82;
        }
    else
        {
            //Check if EPno is between 1 and 9 as these are the only valid eps

            if ((EPReqNo >= 1 && EPReqNo <= 9) && (modules[EPReqNo].EpFlags.EpPresent == 1))
                {

                    {
                    //Simple Desc
                    //set the status to 0x00
                    DestWritePtr =  tempPtr1;
                    *DestWritePtr = 0x00;
                    DestWritePtr += 3;
                    tempPtr1 = DestWritePtr ++;
                    //Copy the EpNO
                    *DestWritePtr++ = EPReqNo;
                    SrcReadPtr = (char*) (&(ZigbeeEpList[1]->profileId));
                    //Copy the profile id, device id and device version
                    for(j = 0; j < 5; j++)
                    {
                        *DestWritePtr++ = *SrcReadPtr++;
                    }
                    //Store the pointer fr IpClusterCnt
                    tempIpClstCntPtr = DestWritePtr++;
                    PktSize += 8;
                    //Find the ip cluster and fill the ids
                    ClusterPtr = ZigbeeEpList[1]->cluster;
                    for (j = 0 ; j < ZigbeeEpList[1]->ClusterCnt; j++,ClusterPtr++)
                    {
                        if (modules[EPReqNo].InClustFlags.flag & mask != 0)
                        {
                            SrcReadPtr =(char*) (& (*ClusterPtr)->clusterID);
                            *DestWritePtr++ = *SrcReadPtr++;
                            *DestWritePtr++ = *SrcReadPtr++;
                            PktSize +=2;
                            ipClustCnt++;
                        }
                        mask = mask << 1;
                    }
                    //Store the pointer fr OpClusterCnt
                    tempOpClstCntPtr = DestWritePtr++;
                    PktSize ++;
                    //Find the ip cluster and fill the ids
                    ClusterPtr = ZigbeeEpList[1]->cluster;
                    mask = 0x01;
                    for (j = 0 ; j < ZigbeeEpList[1]->ClusterCnt; j++,ClusterPtr++)
                    {
                        if(modules[EPReqNo].OutClustFlags.flag & mask != 0)
                        {
                            SrcReadPtr = (char*)(&((*ClusterPtr)->clusterID));
                            *DestWritePtr++ = *SrcReadPtr++;
                            *DestWritePtr++ = *SrcReadPtr++;
                            PktSize +=2;
                            opClustCnt++;
                        }
                        mask = mask << 1;
                    }
                     DestWritePtr =  tempPtr1;
                    *DestWritePtr = PktSize - 5;
                    DestWritePtr = tempIpClstCntPtr;
                    *DestWritePtr = ipClustCnt;
                    DestWritePtr = tempOpClstCntPtr;
                    *DestWritePtr = opClustCnt;
                    }
                }
            else
                {
                    *DestWritePtr++ = 0;
                    PktSize ++;
                    DestWritePtr =  tempPtr1;
                    *DestWritePtr = 0x83;
                }
        }
    ZigbeeTxFrame.Length = PktSize;
    SendExplicitAddPkt(&ZigbeeTxFrame);
#endif
}
#endif
