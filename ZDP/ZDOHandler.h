#ifndef __ZDOHANDLER_H_INCLUDED
#define __ZDOHANDLER_H_INCLUDED
#ifdef USE_ZDO
#include <pic16/stdio.h>
#ifdef USE_USB_LIB
#include <USBCore.h>
#endif // USE_USB_LIB
#include <XbeeTypeDef.h>
#include <ZigbeeGeneralCommands.h>
#include <ZigbeeExternVar.h>
extern void ZDOHandler(ExplicitRxPktType *ZigbeeData);
extern void SimpleDescReq(ExplicitRxPktType *ZigbeeData);
extern void ActiveEpReq(ExplicitRxPktType *ZigbeeData);
extern void MatchDescriptorReq(ExplicitRxPktType *ZigbeeData);
#endif // USE_ZDO
#endif // __ZDOHANDLER_H_INCLUDED
