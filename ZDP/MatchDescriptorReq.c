#include <ZDOHandler.h>

#ifdef USE_ZDO
void MatchDescriptorReq(ExplicitRxPktType *ZigbeeData)
{
#if 1
    unsigned char mask = 0x01;
    unsigned char * DestWritePtr, *SrcReadPtr, *tempStatus, *tempLengthPtr,*tempReadPtr;
    unsigned char PktSize, Found = 0, ReqClustCnt, dirTest, ReqClustLoop, DBClustLoop, DBEpLoop;
    unsigned char DBClustCnt, EPFoundCnt = 0, statusFlag = 0x81;
    unsigned int ReqProfileId, *tempIntPtr, ReqClust;
    genericClusterType  **ClusterPtr;
//    __code genericZigbeeEpType *__code *EPpointer;

    FillHeader(ZigbeeData, 1);
    ZigbeeTxFrame.clusterId = 0x0680;
    DestWritePtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadPtr = ZigbeeData->RxData_Checksum;
    *DestWritePtr ++ = *SrcReadPtr ++; //Copy the Id
    tempStatus = DestWritePtr ++;    //Skip the status will write later and store lcoation
    //    Copy Nwk Address
    *DestWritePtr ++ = *SrcReadPtr ++;
    *DestWritePtr ++ = *SrcReadPtr ++;
    tempLengthPtr = DestWritePtr ++ ;   //Skip the lengh wil be filled later
    PktSize = 5;

    tempIntPtr = (int*)SrcReadPtr;
    SrcReadPtr += 2;    //Two bytes in size
    ReqProfileId = *tempIntPtr;
//    EPpointer = ZigbeeEpList;

    if((ZigbeeEpList[1])->profileId == ReqProfileId)
        {
            Found = 1;
        }

    //Check if the profile is found and if yes then read the byte which is the no of clust
    if(Found == 1)
        {
//            EPpointer = ZigbeeEpList;
//            EPpointer ++;
            tempReadPtr = SrcReadPtr;
            for(DBEpLoop = 1 ; DBEpLoop < ZIGBEE_MAX_ENDPOINT; DBEpLoop++)
                {
                    if(modules[DBEpLoop].EpFlags.EpPresent == 0)
                    {
                        continue;
                    }
                    Found = 0;
                    SrcReadPtr = tempReadPtr;
                    //Repeat the loop twice. Once for input and once for output
                    for(dirTest = 0 ; dirTest < 2; dirTest++)
                        {
                            ReqClustCnt = *SrcReadPtr++;
                            for(ReqClustLoop = 0 ; ReqClustLoop < ReqClustCnt; ReqClustLoop++)
                                {
                                    tempIntPtr = (int*)SrcReadPtr;
                                    SrcReadPtr += 2;
                                    ReqClust = *tempIntPtr;
                                    ClusterPtr = ZigbeeEpList[1]->cluster;
                                    DBClustCnt = ZigbeeEpList[1]->ClusterCnt;
                                    mask = 0x01;
                                    for(DBClustLoop = 0; DBClustLoop < DBClustCnt; DBClustLoop++,ClusterPtr++)
                                        {
                                            if(dirTest == 1)
                                                {
                                                    if((modules[DBEpLoop].OutClustFlags.flag & mask != 0) && (ReqClust == (*ClusterPtr)->clusterID) )
                                                        {
                                                            Found = 1;
                                                        }
                                                }
                                            else
                                                {
                                                    if((modules[DBEpLoop].InClustFlags.flag & mask != 0) && (ReqClust == (*ClusterPtr)->clusterID) )
                                                        {
                                                            Found = 1;
                                                        }
                                                }
                                                mask = mask << 1;
                                        }
                                }
                        }
                        if(Found == 1)
                        {
                            statusFlag = 0x00;
                            *DestWritePtr++ = DBEpLoop;
                            EPFoundCnt++;
                            PktSize ++;
                        }
                }

        }
        DestWritePtr = tempStatus;
        *DestWritePtr = statusFlag;
        DestWritePtr = tempLengthPtr;
        *DestWritePtr = PktSize - 5;
        ZigbeeTxFrame.Length = PktSize;
        SendExplicitAddPkt(&ZigbeeTxFrame);
#endif
}
#endif
