#include <ZDOHandler.h>
#ifdef USE_ZDO

void ZDOHandler(ExplicitRxPktType *ZigbeeData)
{
    unsigned char * DestWritePtr, *SrcReadPtr;
    unsigned char PktSize;

    DestWritePtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadPtr = ZigbeeData->RxData_Checksum;
    switch(ZigbeeData->clusterId)
        {
        case 0x0004:
            SimpleDescReq(ZigbeeData);
            break;
        case 0x0005:
            ActiveEpReq(ZigbeeData);
            break;
        case 0x0006:
            MatchDescriptorReq(ZigbeeData);
            break;
        case 0x0013:
            break;
        default:
            FillHeader(ZigbeeData, 1);
            //Copy the first data
            *DestWritePtr ++ = *SrcReadPtr ++; //Copy the Id
            *DestWritePtr ++= 0x85;   //Unknown Command
            //Copy the NWK Address (Mostly will be Nwk Address)
            *DestWritePtr ++ = *SrcReadPtr ++;
            *DestWritePtr ++ = *SrcReadPtr ++;
            *DestWritePtr = 0x00;   //payload size
            PktSize = 5;
            ZigbeeTxFrame.Length = PktSize;
            SendExplicitAddPkt(&ZigbeeTxFrame);
        }
}
#endif
