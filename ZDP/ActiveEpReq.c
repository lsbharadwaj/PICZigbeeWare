#include <ZDOHandler.h>
#ifdef USE_ZDO
void ActiveEpReq(ExplicitRxPktType *ZigbeeData)
{
    unsigned char * DestWritePtr, *SrcReadPtr;
    unsigned char PktSize,j;
    unsigned char cnt = 0;
//    __code genericZigbeeEpType *__code *EPpointer;
    FillHeader(ZigbeeData, 1);
    ZigbeeTxFrame.clusterId = 0x0580;
    DestWritePtr = ZigbeeTxFrame.TxData_Checksum;
    SrcReadPtr = ZigbeeData->RxData_Checksum;
    *DestWritePtr ++ = *SrcReadPtr ++; //Copy the Id
    *DestWritePtr ++ = 0x00;    //Status is always Success
//    Copy Nwk Address
    *DestWritePtr ++ = *SrcReadPtr ++;
    *DestWritePtr ++ = *SrcReadPtr ++;
    PktSize = 5;
    //Count of the Ep
    *DestWritePtr++ = ZIGBEE_MAX_ENDPOINT;
//    EPpointer = ZigbeeEpList;
    for (j = 0 ; j < ZIGBEE_MAX_ENDPOINT; j++)
    {
        if(modules[j].EpFlags.EpPresent == 1)
        {
        cnt ++;
        *DestWritePtr++ = j;
        PktSize ++;
        }
    }
    DestWritePtr -= cnt + 1;
    *DestWritePtr = cnt;
    ZigbeeTxFrame.Length = PktSize;
    SendExplicitAddPkt(&ZigbeeTxFrame);
}
#endif
