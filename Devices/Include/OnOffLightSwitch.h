#ifndef __ONOFFLIGHTSWITCH_H_INCLUDED
#define __ONOFFLIGHTSWITCH_H_INCLUDED
#include <ClusterOnOff.h>
#include <IdentifyCluster.h>
#include <ZigbeeExternVar.h>
#include <ZigbeeTypeDef.h>
#include <flashAccess.h>
extern void CommissioningPeriodic();
extern void CommissionInitiator(unsigned char btn, unsigned char start);
extern void CommissionTarget(unsigned char btn);
extern void CommissioningReplyHndlr(unsigned char EpNo,unsigned char isZDO, ExplicitRxPktType *ZigbeeData);
#endif // __ONOFFLIGHTSWITCH_H_INCLUDED
